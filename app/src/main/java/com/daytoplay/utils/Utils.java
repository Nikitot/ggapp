package com.daytoplay.utils;

import android.app.Activity;
import android.app.Fragment;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.text.format.Formatter;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.drawee.generic.RoundingParams;
import com.facebook.drawee.view.SimpleDraweeView;
import com.daytoplay.R;
import com.daytoplay.activities.SplashActivity;
import com.daytoplay.fragments.BaseNewsRecyclerFragment;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Utils {

    public static final String SKIPPED = "skipped";
    public static final float ratio9x16 = 0.5625f;
    public static final float ratio16x9 = 1.7778f;
    private static Bitmap glowLikeBitmap;
    private static Typeface typeface;

    public static boolean compareClassName(Class cls1, Class cls2) {
        return cls1.getName().equals(cls2.getName());
    }

    public static DisplayMetrics getDisplayMetrics(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        wm.getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics;
    }

    public static String getYoutubeId(String videoUrl) {
        if (videoUrl == null || videoUrl.trim().length() <= 0)
            return null;
        String reg = "(?:youtube(?:-nocookie)?\\.com\\/(?:[^\\/\\n\\s]+\\/\\S+\\/|(?:v|e(?:mbed)?)\\/|\\S*?[?&]v=)|youtu\\.be\\/)([a-zA-Z0-9_-]{11})";
        Pattern pattern = Pattern.compile(reg, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(videoUrl);

        if (matcher.find())
            return matcher.group(1);
        return null;
    }

    public static Bitmap bitmapFromBytes(@NonNull byte[] byteArray) {
        return BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
    }

    public static byte[] bytesFromBitmap(@NonNull Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        return stream.toByteArray();
    }

    public static String getLocalIp(Context context) {
        WifiManager wifiMgr = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiMgr.getConnectionInfo();
        int ip = wifiInfo.getIpAddress();
        return Formatter.formatIpAddress(ip);
    }

    public static Bitmap bitmapFromDrawable(Drawable drawable, int widthPixels, int heightPixels) {
        Bitmap mutableBitmap = Bitmap.createBitmap(widthPixels, heightPixels, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(mutableBitmap);
        drawable.setBounds(0, 0, widthPixels, heightPixels);
        drawable.draw(canvas);

        return mutableBitmap;
    }

    public static void setSkipSignIn(Context c, boolean skipped) {
        c.getSharedPreferences(c.getString(R.string.private_preferences), Context.MODE_PRIVATE)
                .edit()
                .putBoolean(SKIPPED, skipped)
                .apply();
    }

    public static boolean isSignInSkipped(Context c) {
        SharedPreferences sharedPreferences =
                c.getSharedPreferences(c.getString(R.string.private_preferences), Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(SKIPPED, false);
    }

    private static void sendNotification(Context context, String msg) {

        Intent intent = new Intent(context, SplashActivity.class);
        intent.putExtra("Message", msg);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0,
                intent, PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager
                .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(
                context).setSmallIcon(R.drawable.logo)
                .setContentTitle("CCD Message").setContentText(msg)
                .setAutoCancel(true).setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());

    }

    public static String saveToInternalStorage(Context context, Bitmap bitmapImage) {
        ContextWrapper cw = new ContextWrapper(context);
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath = new File(directory, "iocnnnnnnn1.jpg");

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return directory.getAbsolutePath();
    }

    @Nullable
    public static Object getExtras(Bundle savedInstanceState, Bundle arguments, String key) {
        Object object = null;
        if (savedInstanceState != null && savedInstanceState.containsKey(key)) {
            object = savedInstanceState.get(key);
        }
        if (object == null) {
            object = arguments.get(key);
        }
        return object;
    }

    public static void setBaseRatio(final View view) {
        view.post(new Runnable() {
            @Override
            public void run() {
                ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                float height = view.getWidth() * Utils.ratio9x16;
                layoutParams.height = (int) height;
                view.requestLayout();
            }
        });
    }

    public static int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public static int getNavigationBarHeight(Context context) {
        boolean hasMenuKey = ViewConfiguration.get(context).hasPermanentMenuKey();
        int resourceId = context.getResources().getIdentifier("navigation_bar_height", "dimen", "android");
        if (resourceId > 0 && !hasMenuKey) {
            return context.getResources().getDimensionPixelSize(resourceId);
        }
        return 0;
    }

    public static int dpToPixel(Context context, float dp) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return (int) (dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public static int pixelsToDp(Context context, float px) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return (int) (px / ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public static void toast(@NonNull Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
        Logger.i(context, msg);
    }

    public static Bitmap getRoundedCornerBackground(int color, int width, int height, int pixels) {
        Bitmap output = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, width, height);
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, (float) pixels, (float) pixels, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        return output;
    }

    public static boolean isLandscape(Context context) {
        return context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
    }

    public static Bitmap getGlowLikeBitmap(Context context) {
        if (glowLikeBitmap == null) {
            BitmapDrawable drawable = (BitmapDrawable) context.getResources().getDrawable(R.drawable.ic_favorite_white_24dp);
            if (drawable != null) {
                glowLikeBitmap = Utils.glowIcon(context, drawable.getBitmap(), R.color.color_accent, 24);
            }
        }
        return glowLikeBitmap;
    }

    public static List<String> getBackgroundLinks() {
        List<String> result = new ArrayList<>();
        List<String> links = Arrays.asList(
                "https://pp.vk.me/c629630/v629630274/38a6e/k5o5jgr9baU.jpg",
                "https://pp.vk.me/c629630/v629630274/38a78/nQrVQERrTtM.jpg",
                "https://pp.vk.me/c629630/v629630274/38ab9/esvZDGST-BY.jpg",
                "https://pp.vk.me/c629630/v629630274/38a82/WqmqVofn5HE.jpg",
                "https://pp.vk.me/c629630/v629630274/38a8c/EYI7KbR58DE.jpg",
                "https://pp.vk.me/c629630/v629630274/38a9e/wBQDAHk8Mjo.jpg",
                "https://pp.vk.me/c629630/v629630274/38aa7/6IqnuZ1qf8g.jpg",
                "https://pp.vk.me/c629630/v629630274/38ab0/RM10xb9l9a8.jpg",
                "https://pp.vk.me/c629630/v629630274/38a95/OR6KCJgvDLQ.jpg");
        Collections.shuffle(links);
        result.addAll(links);
        return result;
    }

    public static Bitmap glowIcon(Context context, @NonNull Bitmap src, int colorId, int sizeDp) {
        int size = Utils.dpToPixel(context, sizeDp);
        Bitmap bitmap = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        canvas.drawColor(0, PorterDuff.Mode.CLEAR);
        Paint paint = new Paint();
        paint.setMaskFilter(new BlurMaskFilter(10, BlurMaskFilter.Blur.NORMAL));
        int[] offsetXY = new int[2];
        Bitmap bmAlpha = src.extractAlpha(paint, offsetXY);
        paint = new Paint();
        int color = context.getResources().getColor(colorId);
        paint.setColor(color);
        canvas.drawBitmap(bmAlpha, offsetXY[0], offsetXY[1], paint);
        bmAlpha.recycle();
        paint = new Paint();
        paint.setColorFilter(new LightingColorFilter(color, 1));
        canvas.drawBitmap(src, 0, 0, paint);
        return bitmap;
    }

    public static Typeface getDefaultTypeface(Context context) {
        if (typeface == null) {
            typeface = Typeface.createFromAsset(context.getAssets(), "RobotoTTF/Roboto-Bold.ttf");
        }
        return typeface;
    }

    public static boolean isConneted(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    public static Bitmap getBitmapImageView(ImageView backgroundView) {
        BitmapDrawable drawable = (BitmapDrawable) backgroundView.getDrawable();
        return drawable != null ? drawable.getBitmap() : null;
    }

    public static void setAdditionalArguments(Bundle args, Fragment fragment) {
        if (fragment != null && args != null) {
            Bundle arguments = fragment.getArguments();
            arguments.putBundle(BaseNewsRecyclerFragment.ADDITIONAL, args);
        }
    }

    public static String formatCount(int count) {
        String countStr = String.valueOf(count);
        int length = countStr.length();
        if (length > 1) {
            String s = String.valueOf(countStr.charAt(1));
            String common = "";
            if (!s.equals("0")) {
                common = "." + s;
            }

            if (length > 3) {
                return countStr.charAt(0) + common + "K";
            } else if (length > 6) {
                return countStr.charAt(0) + common + "M";
            } else if (length > 9) {
                return "WoW!";
            }
        }
        return countStr;
    }

    public static void roundCrop(SimpleDraweeView draweeView) {
        RoundingParams roundingParams = RoundingParams.fromCornersRadius(5f);
        roundingParams.setBorder(Color.WHITE, 1.0f);
        roundingParams.setRoundAsCircle(true);
        draweeView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        draweeView.getHierarchy().setRoundingParams(roundingParams);
    }

    public static Bitmap takeScreenshot(Activity context) {
        View rootView = context.getWindow().getDecorView().getRootView();
        rootView.setDrawingCacheEnabled(true);
        return rootView.getDrawingCache();
    }

    public static String fromJsonHtml(String input) {
        return input == null ? null :
                fromUnicode(input
                        .replace("\\n", " ")
                        .replace("\\\"", "\"")
                        .replace("\\\'", "\'"))
                        .replaceAll("\"(.+)\"", "$1");
    }

    public static String fromUnicode(String input) {
        StringBuilder sb = new StringBuilder(input);
        /*startIndex is the point where the backslash is found,
         * endIndex is the point where the unicode section ends; e.g \u003e - endIndex=startIndex+6
		 */
        int startIndex = 0, endIndex = 0, val = 0;

        for (int i = 0; i < sb.length() - 1; i++) {

            if (sb.charAt(i) == '\\') { 	/*check if the char is a backslash*/
                startIndex = i; 				/*save the index as a starting point for replace later*/
                endIndex = startIndex + 6;
                if (sb.charAt(++i) == 'u') {/*check if the next char is a 'u' which indicates a unicode section is found*/

					/* extract the unicode section withouth \ u and convert to an integer which is then used to convert to
                     * its respective character
					 */
                    val = Integer.parseInt(sb.substring(++i, i + 4), 16);

                    sb.replace(startIndex, endIndex, String.valueOf((char) (val)));
                    i = startIndex;
                }
            }
        }
        return sb.toString();
    }

    public static String md5(String s) {
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++)
                hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }
}
