package com.daytoplay.utils;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.TextView;

public class AnimationHelper {

    public static void fadeHide(final View v, int duration) {
        v.animate()
                .alpha(0f)
                .setDuration(duration)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        v.setVisibility(View.GONE);
                    }
                })
                .start();
    }

    public static void fadeShow(final View v, int duration) {
        v.setAlpha(0f);
        v.setVisibility(View.VISIBLE);
        v.animate()
                .alpha(1f)
                .setDuration(duration)
                .start();
    }

    public void animateTextSize(final TextView title, int startSize, int endSize, int duration) {
        ValueAnimator animator = ValueAnimator.ofFloat(startSize, endSize);
        animator.setDuration(duration);
        animator.setInterpolator(new AccelerateDecelerateInterpolator());

        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float animatedValue = (float) valueAnimator.getAnimatedValue();
                title.setTextSize(animatedValue);
            }
        });

        animator.start();
    }

    public static boolean isTransitionAnimationAvailable(boolean haveFresco) {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP
               /* && Build.VERSION.SDK_INT < Build.VERSION_CODES.N || !haveFresco*/;
    }

    public static void animateColorView(@NonNull Context context, @NonNull View view, int colorId) {
        Drawable currentBG = view.getBackground();
        Drawable newBG = new ColorDrawable(context.getResources().getColor(colorId));
        if (currentBG == null) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                view.setBackgroundDrawable(newBG);
            } else {
                view.setBackground(newBG);
            }
        } else {
            TransitionDrawable transitionDrawable = new TransitionDrawable(new Drawable[]{currentBG, newBG});
            transitionDrawable.setCrossFadeEnabled(true);
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                view.setBackgroundDrawable(transitionDrawable);
            } else {
                view.setBackground(transitionDrawable);
            }
            transitionDrawable.startTransition(500);
        }
    }
}
