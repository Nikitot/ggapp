package com.daytoplay.utils;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.daytoplay.R;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.crash.FirebaseCrash;

public class Logger {

    public static FirebaseAnalytics firebaseAnalytics;
    private static String firebaseContextName;

    /**
     * Send info log to FirebaseCrash and show when debug version
     *
     * @param context
     * @param msg
     */
    public static void i(Context context, String msg) {
        if (context != null) {
            msg = context.getClass().getName() + " " + msg;
        }
        if (msg == null) {
            msg = "Unknown error";
        }
        FirebaseCrash.log(msg);
        d(context, msg);
    }

    protected static void d(Context context, String msg) {
        if (context != null) {
            Log.d(context.getResources().getString(R.string.app_name), msg);
        }
    }

    /**
     * Send event to FirebaseAnalytics and show when debug version
     *
     * @param context
     * @param event
     * @param params
     */
    public static void s(Context context, String event, Bundle params) {
        if (firebaseAnalytics == null ||
                !firebaseContextName.equals(context.getClass().getName())) {
            createFirebaseAnalytics(context);
        }
        if (firebaseAnalytics != null) {
            firebaseAnalytics.logEvent(event, params);
            d(context, event + " " + params.toString());
        }
    }

    public static void createFirebaseAnalytics(Context context) {
        firebaseAnalytics = FirebaseAnalytics.getInstance(context);
        firebaseContextName = context.getClass().getName();
    }

    /**
     * Send error log to FirebaseCrash and show when debug version
     *
     * @param context
     * @param msg
     */
    public static void e(Context context, String msg) {
        if (context != null) {
            msg = context.getClass().getName() + " " + msg;
        }
        if (msg == null) {
            msg = "Unknown error";
        }
        FirebaseCrash.log(msg);
        if (context != null) {
            Log.e(context.getResources().getString(R.string.app_name), msg);
        }
    }
}
