package com.daytoplay.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.io.InputStream;

public abstract class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {

    protected Bitmap doInBackground(String... urls) {
        String urlDisplay = urls[0];
        Bitmap bitmap = null;
        try {
            InputStream in = new java.net.URL(urlDisplay).openStream();
            bitmap = BitmapFactory.decodeStream(in);
        } catch (Exception e) {
            Logger.e(null, e.getMessage());
        }
        return bitmap;
    }

    protected void onPostExecute(Bitmap result) {
            onBitmapLoaded(result);
    }

    protected abstract  void onBitmapLoaded(Bitmap bitmap);
}