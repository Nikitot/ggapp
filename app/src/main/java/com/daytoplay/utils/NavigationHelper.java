package com.daytoplay.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.daytoplay.R;

public final class NavigationHelper {

    public static void startActivityNoHistory(Context c, Class cls) {
        Intent intent = new Intent(c, cls);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        c.startActivity(intent);
        if (c instanceof Activity) {
            Activity a = (Activity) c;
            if (!a.isFinishing()) {
                a.finish();
            }
        }
    }

    public static void startActivityClearTask(Context c, Class cls) {
        Intent intent = new Intent(c, cls);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        c.startActivity(intent);
        if (c instanceof Activity) {
            Activity a = (Activity) c;
            if (!a.isFinishing()) {
                a.finish();
            }
        }
    }

    public static void shareLink(Context c, String link) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, link);
        sendIntent.setType("text/plain");
        c.startActivity(Intent.createChooser(sendIntent, c.getResources().getText(R.string.send_to)));
    }
}
