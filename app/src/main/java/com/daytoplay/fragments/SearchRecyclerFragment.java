package com.daytoplay.fragments;


import android.os.Bundle;
import android.view.View;

import com.daytoplay.Connector;
import com.daytoplay.ErrorType;
import com.daytoplay.adapters.BaseFeedRecyclerAdapter;
import com.daytoplay.items.NewsContent;

import java.util.List;

import retrofit2.Response;


public class SearchRecyclerFragment extends BaseNewsRecyclerFragment {

    private String term;
    int page = 1;

    protected int getLayoutId() {
        return com.daytoplay.R.layout.refreshable_fragment_recycler;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setVisibilityProgress(false);
    }

    @Override
    protected boolean fillContent() {
        if (!super.fillContent())
            return false;

        if (term != null) {
            Connector.createService()
                    .search(term, page).enqueue(new Connector.Callback<List<NewsContent.NewsItem>>(getActivity()) {
                @Override
                protected void onSuccess(Response<List<NewsContent.NewsItem>> response, String url) {
                    super.onSuccess(response, url);
                    List<NewsContent.NewsItem> items = response.body();
                    if (items != null && items.size() > 0) {
                        if (adapter == null) {
                            setAdapter(new BaseFeedRecyclerAdapter(SearchRecyclerFragment.this, items, listener));
                        }
                        onLoadFinished();
                        page++;
                    } else {
                        SearchRecyclerFragment.this.onError(ErrorType.NOT_FOUND);
                    }
                }

                @Override
                protected void onError(ErrorType errorType, String msg, String url) {
                    super.onError(errorType, msg, url);
                    SearchRecyclerFragment.this.onError(ErrorType.NOT_RECEIVED);
                }
            });
            return true;
        } else {
            onError(ErrorType.SEARCH);
            return false;
        }
    }

    @Override
    protected void onLoadMore(final boolean top, NewsContent.NewsItem newsItem) {
        super.onLoadMore(top, newsItem);
        if (term != null) {
            Connector.createService()
                    .search(term, top ? 1 : page).enqueue(new Connector.Callback<List<NewsContent.NewsItem>>(getActivity()) {
                @Override
                protected void onSuccess(Response<List<NewsContent.NewsItem>> response, String url) {
                    super.onSuccess(response, url);
                    List<NewsContent.NewsItem> items = response.body();
                    if (items != null && items.size() > 0) {
                        updateAdapter(response.body(), top);
                        if (listener != null) {
                            listener.onUpdateFinished();
                        }
                        if(!top) page++;
                    } else {
                        SearchRecyclerFragment.this.onError(ErrorType.NOT_FOUND);
                    }
                }

                @Override
                protected void onError(ErrorType errorType, String msg, String url) {
                    super.onError(errorType, msg, url);
                    SearchRecyclerFragment.this.onError(ErrorType.NOT_RECEIVED);
                }
            });
        } else {
            onError(ErrorType.SEARCH);
        }
    }

    @Override
    protected int getTopOffset() {
        return 0;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    public void setQuery(String term) {
        this.term = term;
        reloadAll();
    }
}
