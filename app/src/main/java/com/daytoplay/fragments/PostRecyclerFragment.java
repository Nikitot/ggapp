package com.daytoplay.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gg.api.PostItem;
import com.daytoplay.ErrorType;
import com.daytoplay.R;
import com.daytoplay.adapters.PostItemRecyclerAdapter;

import java.util.ArrayList;

public class PostRecyclerFragment extends Fragment {
    private static final String CONTENT = "content";
    public static String TAG = PostRecyclerFragment.class.getName();
    RecyclerView recycler;
    @Nullable
    PostItem content;
    private OnContentPartsClickListener listener;
    private PostItemRecyclerAdapter adapter;

    @Nullable
    public static PostRecyclerFragment newInstance(PostItem content) {
        PostRecyclerFragment fragment = new PostRecyclerFragment();
        Bundle args = new Bundle();
        args.putSerializable(CONTENT, content);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Activity activity = getActivity();
        if (activity instanceof OnContentPartsClickListener) {
            listener = (OnContentPartsClickListener) activity;
        } else {
            throw new RuntimeException(activity.getClass().getName()
                    + " must implement " + OnContentPartsClickListener.class.getName());
        }

        Bundle args = savedInstanceState != null ? savedInstanceState : getArguments();

        if (args != null) {
            content = (PostItem) args.getSerializable(CONTENT);
        }

        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View rootView = layoutInflater.inflate(com.daytoplay.R.layout.fragment_recycler, viewGroup, false);
        if (rootView != null) {
            recycler = (RecyclerView) rootView.findViewById(com.daytoplay.R.id.recycler);
            recycler.setNestedScrollingEnabled(false);
            recycler.setLayoutManager(new LinearLayoutManager(getActivity()));
            adapter = new PostItemRecyclerAdapter(getActivity(), content, listener);
            recycler.setAdapter(adapter);
            recycler.addItemDecoration(new RecyclerView.ItemDecoration() {

                @Override
                public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                    super.getItemOffsets(outRect, view, parent, state);
                    int last = adapter.getItemCount() - 1;

                    int type = content != null ? content.getList().get(last).getType() : -1;

                    if (parent.getChildAdapterPosition(view) == last) {
                        int bottom = genNavigationBarHeight();
                        if (type == PostItem.TEXT) {
                            bottom += (int) getResources().getDimension(com.daytoplay.R.dimen.bottom_margin);
                        } else if (type == PostItem.IMAGES) {
                            bottom += (int) getResources().getDimension(R.dimen.bottom_margin_small);
                        }
                        outRect.bottom = bottom;
                    }
                }
            });
        }
        return rootView;
    }

    private int genNavigationBarHeight() {
        Resources resources = getResources();
        int resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android");
        if (resourceId > 0) {
            return resources.getDimensionPixelSize(resourceId);
        }
        return 0;
    }

    public void removeVideoContent(boolean updatePlayerThumbnails) {
        ((PostItemRecyclerAdapter) recycler.getAdapter()).releasePlayers(updatePlayerThumbnails);
    }

    public void restoreVideoPlayer(String id, int position) {
        ((PostItemRecyclerAdapter) recycler.getAdapter()).restorePlayer(id, position);
    }


    public interface OnContentPartsClickListener {
        void onPhotoClicked(View v, ArrayList<String> itemView, int url);

        void onError(ErrorType errorType);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(CONTENT, content);
        super.onSaveInstanceState(outState);
    }
}
