package com.daytoplay.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gg.api.Channel;
import com.daytoplay.Connector;
import com.daytoplay.ErrorType;
import com.daytoplay.activities.ChannelActivity;
import com.daytoplay.adapters.ChannelFeedRecyclerAdapter;
import com.daytoplay.items.NewsContent;

import java.util.List;

import retrofit2.Response;


public class ChannelNewsRecyclerFragment extends BaseNewsRecyclerFragment {

    private Channel channel;

    @Nullable
    public static ChannelNewsRecyclerFragment newInstance(Channel channel) {
        ChannelNewsRecyclerFragment fragment = new ChannelNewsRecyclerFragment();
        Bundle args = new Bundle();
        args.putSerializable(ChannelActivity.CHANNEL, channel);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        channel = (Channel) getArguments().getSerializable(ChannelActivity.CHANNEL);
        return view;
    }

    @Override
    protected boolean fillContent() {
        if (!super.fillContent() && channel != null) return false;
        Connector.createService()
                .getNewsByChannel(channel.getId(), page).enqueue(new Connector.Callback<List<NewsContent.NewsItem>>(getActivity()) {
            @Override
            protected void onSuccess(Response<List<NewsContent.NewsItem>> response, String url) {
                super.onSuccess(response, url);
                List<NewsContent.NewsItem> items = response.body();
                if (items != null && items.size() > 0) {
                    if (adapter == null) {
                        setAdapter(new ChannelFeedRecyclerAdapter(ChannelNewsRecyclerFragment.this, items, listener));
                    }
                    onLoadFinished();
                    page++;
                } else {
                    ChannelNewsRecyclerFragment.this.onError(ErrorType.NO_ONE);
                }
            }

            @Override
            protected void onError(ErrorType errorType, String msg, String url) {
                super.onError(errorType, msg, url);
                ChannelNewsRecyclerFragment.this.onError(errorType);
            }
        });
        return true;
    }

    @Override
    public void onDataReset() {
        super.onDataReset();
        page = 1;
    }

    @Override
    protected void onLoadMore(final boolean top, NewsContent.NewsItem newsItem) {
        super.onLoadMore(top, newsItem);
        Connector.createService()
                .getNewsByChannel(channel.getId(), top ? 1 : page).enqueue(new Connector.Callback<List<NewsContent.NewsItem>>(getActivity()) {
            @Override
            protected void onSuccess(Response<List<NewsContent.NewsItem>> response, String url) {
                super.onSuccess(response, url);
                List<NewsContent.NewsItem> items = response.body();
                if (items != null && items.size() > 0) {
                    updateAdapter(response.body(), top);
                    if (listener != null) {
                        listener.onUpdateFinished();
                    }
                    if(!top) page++;
                }
            }

            @Override
            protected void onError(ErrorType errorType, String msg, String url) {
                super.onError(errorType, msg, url);
                ChannelNewsRecyclerFragment.this.onError(ErrorType.NOT_RECEIVED);
            }
        });
    }
}
