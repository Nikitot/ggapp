package com.daytoplay.fragments;

import com.daytoplay.R;

public class TopNewsRecyclerFragment extends BaseNewsRecyclerFragment {

    @Override
    protected boolean fillContent() {
        return super.fillContent();
    }

    @Override
    public void updateContent(boolean top) {
        super.updateContent(top);
    }

    protected int getLayoutId() {
        return R.layout.refreshable_fragment_recycler;
    }
}
