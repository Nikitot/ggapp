package com.daytoplay.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gg.api.Channel;
import com.daytoplay.Config;
import com.daytoplay.Connector.Service.UpdateType;
import com.daytoplay.ErrorType;
import com.daytoplay.R;
import com.daytoplay.activities.FeedActivity;
import com.daytoplay.activities.FeedActivity.SectionType;
import com.daytoplay.adapters.BaseFeedRecyclerAdapter;
import com.daytoplay.items.NewsContent;
import com.daytoplay.items.NewsContent.NewsItem;
import com.daytoplay.utils.Utils;
import com.daytoplay.views.FeedItemHolder;

import java.lang.ref.WeakReference;
import java.util.List;

import static com.daytoplay.Connector.Service.AFTER;
import static com.daytoplay.Connector.Service.BEFORE;

public abstract class BaseNewsRecyclerFragment extends Fragment implements
        SwipeRefreshLayout.OnRefreshListener {

    public static final int NOT_RECEIVED = 1;
    public static final String TYPE = SectionType.class.getName();
    public static final String ADDITIONAL = "additional";
    @Nullable
    protected BaseFeedRecyclerAdapter adapter;

    protected SectionType type;
    protected SwipeRefreshLayout swipeRefreshLayout;
    @Nullable
    protected RecyclerListener listener;

    private MessageHandler messageHandler = new MessageHandler(this);
    private RecyclerView recyclerView;
    private View progressBar;
    protected LinearLayout updateLayout;
    private Button updateButton;
    private TextView errorView;
    @Nullable
    protected Bundle additional;
    private boolean first = true;
    protected int page = 1;
    private RecyclerView.ItemDecoration decor;

    @Nullable
    public static BaseNewsRecyclerFragment newInstance(@NonNull FeedActivity.SectionType type) {
        BaseNewsRecyclerFragment fragment;
        switch (type) {
            case NEW:
                fragment = new HeadlinesNewsRecyclerFragment();
                break;
            case LIKED:
                fragment = new LikeNewsRecyclerFragment();
                break;
            default:
                fragment = new EmptyNewsRecyclerFragment();
        }
        Bundle args = new Bundle();
        args.putSerializable(TYPE, type);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Activity activity = getActivity();
        if (activity instanceof RecyclerListener) {
            listener = (RecyclerListener) activity;
        } else {
            throw new RuntimeException(activity.getClass().getName()
                    + " must implement RecyclerListener");
        }

        if (savedInstanceState != null) {
            additional = savedInstanceState.getBundle(ADDITIONAL);
            type = (SectionType) savedInstanceState.getSerializable(TYPE);
        } else {
            Bundle arguments = getArguments();
            if (arguments != null) {
                additional = arguments.getBundle(ADDITIONAL);
                type = (SectionType) arguments.getSerializable(TYPE);
            }
        }

        setRetainInstance(true);
    }

    protected int getTopOffset() {
        Resources r = getResources();
        return +r.getDimensionPixelSize(R.dimen.section_navigation_height) - r.getDimensionPixelSize(R.dimen.section_navigation_bottom_offset);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(getLayoutId(), container, false);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler);
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.refresh);
        swipeRefreshLayout.setProgressViewOffset(true, 0,
                getTopOffset() + getResources().getDimensionPixelSize(R.dimen.swipe_refresh_offset));
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(R.color.color_accent);

        updateLayout = (LinearLayout) rootView.findViewById(R.id.update_layout);
        progressBar = updateLayout.findViewById(R.id.progress);
        updateButton = (Button) updateLayout.findViewById(R.id.update_button);
        errorView = (TextView) updateLayout.findViewById(R.id.error);

        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reloadAll();
            }
        });

        setColumnCount(recyclerView.getContext(), recyclerView, 1);

        return rootView;
    }


    protected void reloadAll() {
        showErrorLayout(false);
        fillContent();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (!(this instanceof SearchRecyclerFragment)) {
            fillContent();
        }
    }

    protected boolean fillContent() {
        if (Config.hasUser()) {
            Activity activity = getActivity();
            if (activity instanceof FeedActivity) {
                if (((FeedActivity) activity).isConnected()) {
                    int delay;
                    if (first) {
                        delay = 15000;
                        first = false;
                    } else {
                        delay = 30000;
                    }
                    messageHandler.sendEmptyMessageDelayed(NOT_RECEIVED, delay);
                } else {
                    onError(ErrorType.NO_CONNECTION);
                    return false;
                }
            }
        } else {
            if (type == SectionType.LIKED)
                showStubNoUser();
        }
        return true;
    }

    private void showStubNoUser() {
        updateLayout.removeAllViews();
        final Activity activity = getActivity();
        ImageView imageStub = new ImageView(activity);
        updateLayout.addView(imageStub);
        imageStub.setImageResource(R.drawable.dog);
        imageStub.setLayoutParams(new LinearLayout.LayoutParams(700, 700));
        imageStub.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
    }

    public void updateContent(boolean top) {
        if (adapter != null && adapter.getItemCount() > 0) {
            if (top) {
                onLoadMore(true, adapter.getItem(0));
            } else {
                onLoadMore(false, adapter.getItem(adapter.getItems().size() - 1));
            }
        } else {
            fillContent();
        }
    }

    protected void onLoadMore(final boolean top, NewsContent.NewsItem newsItem) {
        if (newsItem == null) return;
        if (listener != null) {
            listener.onUpdateStarted(top);
        }
    }

    protected void setAdapter(@NonNull BaseFeedRecyclerAdapter adapter) {
        this.adapter = adapter;
        recyclerView.setAdapter(adapter);
    }

    protected void updateAdapter(@NonNull List<NewsItem> items, boolean top) {
        if (adapter != null) {
            adapter.addItems(items, top);
            if (top && adapter.getLastBindPosition() < 4) {
                recyclerView.smoothScrollToPosition(0);
            }
        }
        updateItemDecorator();
    }

    private void showErrorLayout(boolean showError) {
        updateLayout.setVisibility(View.VISIBLE);
        swipeRefreshLayout.setVisibility(View.GONE);
        if (showError) {
            setVisibilityProgress(false);
            updateButton.setVisibility(View.VISIBLE);
            errorView.setVisibility(View.VISIBLE);
        } else {
            setVisibilityProgress(true);
            updateButton.setVisibility(View.GONE);
            errorView.setVisibility(View.GONE);
        }
    }

    protected void setVisibilityProgress(boolean visibility) {
        if (visibility) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

    private void hideInfoLayout() {
        swipeRefreshLayout.setVisibility(View.VISIBLE);
        updateLayout.setVisibility(View.GONE);
    }

    protected void onLoadFinished() {
        messageHandler.removeMessages(NOT_RECEIVED);
        hideInfoLayout();
        updateItemDecorator();
    }

    private void updateItemDecorator() {
        if (adapter != null) {
            recyclerView.removeItemDecoration(decor);
            decor = new RecyclerView.ItemDecoration() {
                @Override
                public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                    super.getItemOffsets(outRect, view, parent, state);

                    outRect.bottom = parent.getChildAdapterPosition(view) == adapter.getItemCount() - 1 ?
                            Utils.getNavigationBarHeight(getActivity()) : 0;
                }
            };
            recyclerView.addItemDecoration(decor);
        }
    }

    protected void onError(ErrorType error) {
        if (listener != null) {
            listener.onUpdateFinished();
        }
        try {
            messageHandler.removeMessages(NOT_RECEIVED);
            errorView.setText(getResources().getText(error.textId));
            if (adapter != null && adapter.getItemCount() != 0) {
                hideInfoLayout();
            } else {
                showErrorLayout(true);
            }
        } catch (IllegalStateException ex) {
            ex.printStackTrace();
        }
    }

    private void setColumnCount(Context context, RecyclerView recyclerView, int columnCount) {
        if (columnCount <= 1) {
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(context, columnCount));
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        updateContent(true);
        swipeRefreshLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
            }
        }, 2000);
    }


    @UpdateType
    @SuppressLint("SupportAnnotationUsage")
    protected char getSign(boolean top) {
        return top ? AFTER : BEFORE;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBundle(ADDITIONAL, additional);
        outState.putSerializable(TYPE, type);
        super.onSaveInstanceState(outState);
    }

    public void notifyItem(int i) {
        if (adapter != null) {
            adapter.notifyItemChanged(i);
        }
    }

    @Nullable
    public BaseFeedRecyclerAdapter getAdapter() {
        return adapter;
    }

    public void onDataReset() {
        page = 1;
    }

    private static class MessageHandler extends Handler {

        private final WeakReference<BaseNewsRecyclerFragment> fragment;

        MessageHandler(BaseNewsRecyclerFragment view) {
            fragment = new WeakReference<>(view);
        }

        @Override
        public void handleMessage(Message msg) {
            BaseNewsRecyclerFragment fragment = this.fragment.get();
            switch (msg.what) {
                case (NOT_RECEIVED):
                    if (fragment != null) {
                        fragment.onError(ErrorType.TIME_OUT);
                        fragment.showErrorLayout(true);
                    }
                    break;
                default:
                    super.handleMessage(msg);
                    break;
            }
        }
    }

    protected int getLayoutId() {
        return R.layout.refreshable_fragment_recycler;
    }

    public interface RecyclerListener {
        void onRecyclerItemClicked(FeedItemHolder itemHolder);

        void onShareClicked(String link);

        void onChannelClicked(Channel id);

        void onUpdateStarted(boolean top);

        void onUpdateFinished();
    }
}
