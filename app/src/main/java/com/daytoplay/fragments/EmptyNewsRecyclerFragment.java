package com.daytoplay.fragments;

import com.daytoplay.R;

public class EmptyNewsRecyclerFragment extends BaseNewsRecyclerFragment {

    protected int getLayoutId() {
        return R.layout.refreshable_fragment_recycler;
    }
}
