package com.daytoplay.fragments;


import com.gg.api.Channel;
import com.gg.api.Utils;
import com.daytoplay.Config;
import com.daytoplay.Connector;
import com.daytoplay.ErrorType;
import com.daytoplay.adapters.NewFeedRecyclerAdapter;
import com.daytoplay.items.NewsContent;

import java.util.List;

import retrofit2.Call;
import retrofit2.Response;


public class HeadlinesNewsRecyclerFragment extends BaseNewsRecyclerFragment {

    @Override
    protected boolean fillContent() {
        if (!super.fillContent()) return false;
        Connector.createService().getChannels().enqueue(new Connector.Callback<List<Channel>>(getActivity()) {
            @Override
            protected void onSuccess(Response<List<Channel>> response, String method) {
                super.onSuccess(response, method);
                final List<Channel> channels = response.body();
                Config.get().channels.addAll(channels);
                Connector.createService().getNews().enqueue(new Connector.Callback<List<NewsContent.NewsItem>>(getActivity()) {
                    @Override
                    protected void onSuccess(Response<List<NewsContent.NewsItem>> response, String url) {
                        super.onSuccess(response, url);
                        List<NewsContent.NewsItem> items = response.body();
                        if (items != null && items.size() > 0) {
                            setAdapter(new NewFeedRecyclerAdapter(HeadlinesNewsRecyclerFragment.this, items, channels, listener));
                            onLoadFinished();
                        } else {
                            HeadlinesNewsRecyclerFragment.this.onError(ErrorType.NO_ONE);
                        }
                    }

                    @Override
                    protected void onError(ErrorType errorType, String msg, String url) {
                        super.onError(errorType, msg, url);
                        HeadlinesNewsRecyclerFragment.this.onError(errorType);
                    }
                });
            }
        });
        return true;
    }

    @Override
    protected void onLoadMore(final boolean top, NewsContent.NewsItem newsItem) {
        super.onLoadMore(top, newsItem);
        if (adapter != null) {
            Call<List<NewsContent.NewsItem>> call = Connector.createService()
                    .getNews(getSign(top), Utils.excludingTomeZone(newsItem.created_at));
            if (call != null) {
                call.enqueue(new Connector.Callback<List<NewsContent.NewsItem>>(getActivity()) {
                    @Override
                    protected void onSuccess(Response<List<NewsContent.NewsItem>> response, String url) {
                        super.onSuccess(response, url);
                        List<NewsContent.NewsItem> items = response.body();
                        if (items != null && items.size() > 0) {
                            updateAdapter(response.body(), top);
                            if (listener != null) {
                                listener.onUpdateFinished();
                            }
                        } else {
                            HeadlinesNewsRecyclerFragment.this.onError(ErrorType.NO_ONE);
                        }
                    }

                    @Override
                    protected void onError(ErrorType errorType, String msg, String url) {
                        super.onError(errorType, msg, url);
                        HeadlinesNewsRecyclerFragment.this.onError(errorType);
                    }
                });
            } else {
                onError(ErrorType.ERROR);
            }
        }
    }
}
