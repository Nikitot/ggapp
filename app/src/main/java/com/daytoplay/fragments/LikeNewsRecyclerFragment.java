package com.daytoplay.fragments;

import com.daytoplay.Config;
import com.daytoplay.Connector;
import com.daytoplay.ErrorType;
import com.daytoplay.R;
import com.daytoplay.adapters.LikedFeedRecyclerAdapter;
import com.daytoplay.items.LikeItem;
import com.daytoplay.items.NewsContent;

import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class LikeNewsRecyclerFragment extends BaseNewsRecyclerFragment {

    @Override
    protected boolean fillContent() {
        if (!super.fillContent()) return false;
        if (Config.hasUser()) {
            Connector.createService().getLiked(Config.auth.getCurrentUser().getUid()).enqueue(
                    new Connector.Callback<List<NewsContent.NewsItem>>(getActivity()) {
                        @Override
                        protected void onSuccess(Response<List<NewsContent.NewsItem>> response, String url) {
                            super.onSuccess(response, url);
                            List<NewsContent.NewsItem> items = response.body();
                            if (items != null && items.size() > 0) {
                                setAdapter(new LikedFeedRecyclerAdapter(LikeNewsRecyclerFragment.this, items, listener));
                                onLoadFinished();
                            } else {
                                LikeNewsRecyclerFragment.this.onError(ErrorType.NO_ONE);
                            }
                        }

                        @Override
                        protected void onError(ErrorType errorType, String msg, String url) {
                            super.onError(errorType, msg, url);
                            LikeNewsRecyclerFragment.this.onError(errorType);
                        }
                    });
        }
        return true;
    }

    protected void onLoadMore(final boolean top, NewsContent.NewsItem newsItem) {
        super.onLoadMore(top, newsItem);
        Connector.createService().getLike(newsItem.id, Config.auth.getCurrentUser().getUid()).enqueue(new Connector.Callback<List<LikeItem>>(getActivity()) {
            @Override
            protected void onSuccess(Response<List<LikeItem>> response, String url) {
                super.onSuccess(response, url);
                List<LikeItem> body = response.body();
                if (body.size() > 0) {
                    LikeItem likeItem = body.get(0);
                    Call<List<NewsContent.NewsItem>> call = Connector.createService()
                            .getLiked(Config.auth.getCurrentUser().getUid(), getSign(top), com.gg.api.Utils.excludingTomeZone(likeItem.created_at));
                    if (call != null) {
                        call.enqueue(new Connector.Callback<List<NewsContent.NewsItem>>(getActivity()) {
                            @Override
                            protected void onSuccess(Response<List<NewsContent.NewsItem>> response, String url) {
                                super.onSuccess(response, url);
                                List<NewsContent.NewsItem> items = response.body();
                                if (items != null && items.size() > 0) {
                                    updateAdapter(response.body(), top);
                                    if (listener != null) {
                                        listener.onUpdateFinished();
                                    }
                                } else {
                                    LikeNewsRecyclerFragment.this.onError(ErrorType.NO_ONE);
                                }
                            }

                            @Override
                            protected void onError(ErrorType errorType, String msg, String url) {
                                super.onError(errorType, msg, url);
                                LikeNewsRecyclerFragment.this.onError(errorType);
                            }
                        });
                    } else {
                        LikeNewsRecyclerFragment.this.onError(ErrorType.ERROR);
                    }
                }
            }
        });
    }

    protected int getLayoutId() {
        return R.layout.refreshable_fragment_recycler;
    }
}
