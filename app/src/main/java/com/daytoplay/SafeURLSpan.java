package com.daytoplay;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.text.Html;
import android.text.Spannable;
import android.text.style.URLSpan;
import android.view.View;
import android.webkit.URLUtil;

import com.daytoplay.activities.PostActivity;


@SuppressLint("ParcelCreator")
public final class SafeURLSpan extends URLSpan {
    public SafeURLSpan(String url) {
        super(url);
    }

    @Override
    public void onClick(View view) {
        try {
            Activity activity = (Activity) view.getContext();
            if (activity != null) {
                String url = getURL();
                String prefix = "/click/?";
                if (url.contains(prefix)) {
                    url = url.substring(prefix.length());
                }
                if (!URLUtil.isValidUrl(url)) {
                    String rootUrl = (String) ((PostActivity) activity).getRootLink();
                    url = rootUrl + url;
                }
                if (URLUtil.isValidUrl(url)) {
                    startExternalWebActivity(activity, url);
                }
            }
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
    }

    private void startExternalWebActivity(Activity activity, String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    public static CharSequence parseSafeHtml(CharSequence html) {
        return replaceURLSpans(Html.fromHtml(html.toString()));
    }

    public static CharSequence replaceURLSpans(CharSequence text) {
        if (text instanceof Spannable) {
            final Spannable s = (Spannable) text;
            final URLSpan[] spans = s.getSpans(0, s.length(), URLSpan.class);
            if (spans != null && spans.length > 0) {
                for (int i = spans.length - 1; i >= 0; i--) {
                    final URLSpan span = spans[i];
                    final int start = s.getSpanStart(span);
                    final int end = s.getSpanEnd(span);
                    final int flags = s.getSpanFlags(span);
                    s.removeSpan(span);
                    s.setSpan(new SafeURLSpan(span.getURL()), start, end, flags);
                }
            }
        }
        return text;
    }
}
