package com.daytoplay;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.IntDef;
import android.util.Pair;

import com.gg.api.Channel;
import com.gg.api.PostPartItem;
import com.daytoplay.items.LikeItem;
import com.daytoplay.items.NewsContent;
import com.daytoplay.utils.Logger;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public class Connector {
    public static int port = Config.getPort();
    public static String host = "server.noorsoft.ru";

    private static Retrofit retrofit;

    public static Service createService() {
        if (retrofit == null) {
            String port = "";
            if (Connector.port != -1) {
                port = ":" + Connector.port;
            }
            retrofit = new Retrofit.Builder()
                    .baseUrl("http://" + host + port + "/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit.create(Service.class);
    }

    public static void resetPort(int port) {
        Connector.port = port;
        retrofit = null;
    }

    public static HashMap<String, String> createBody(Pair<String, String> value) {
        HashMap<String, String> body = new HashMap<>(1);
        body.put(value.first, value.second);
        return body;
    }

    public interface Service {

        @Retention(RetentionPolicy.SOURCE)
        @IntDef({BEFORE, AFTER})
        @interface UpdateType {
        }

        char BEFORE = '<';
        char AFTER = '>';

        @GET("news_items")
        Call<List<NewsContent.NewsItem>> getNews();

        @GET("news_items/all_by_channel")
        Call<List<NewsContent.NewsItem>> getNewsByChannel(@Query("channel_id") long id,
                                                          @Query("page") int page);

        @GET("news_items/search_by_date")
        @SuppressLint("SupportAnnotationUsage")
        Call<List<NewsContent.NewsItem>> getNews(@Query("criteria") @UpdateType char sign,
                                                 @Query("date") Timestamp time);

        @GET("news_items/suggestions")
        Call<List<String>> getSuggestion(@Query("title") String term);

        @GET("news_items/{id}/")
        Call<List<PostPartItem>> getPostItem(@Path("id") long id);

        @GET("search")
        Call<List<NewsContent.NewsItem>> updateSearchById(@Query("id") long id,
                                                          @Query("term") String msg);

        @Headers("Content-Type: application/json")
        @POST("client_registration_tokens")
        Call<ResponseBody> registerToken(@Body String jsonToken);

        @Headers("Content-Type: application/json")
        @POST("")
        Call<ResponseBody> subscribeChannel(@Body HashMap<String, String> user_channel);

        @GET("news_items/search")
        Call<List<NewsContent.NewsItem>> search(@Query("title") String term, @Query("page") int page);

        @GET("news_item_user_likes/search")
        Call<List<LikeItem>> getLike(@Query("news_item_id") long newsItemId, @Query("user_id") String userId);

        @Headers("Content-Type: application/json")
        @POST("news_item_user_likes")
        Call<ResponseBody> like(@Body HashMap<String, Object> body);

        @DELETE("news_item_user_likes/{id}")
        Call<ResponseBody> unlike(@Path("id") long likeId);

        @GET("channels/{id}")
        Call<Channel> getChannel(@Path("id") int id);

        @GET("/channels")
        Call<List<Channel>> getChannels();

        @GET("/news_item_user_likes/search_news_by_user")
        Call<List<NewsContent.NewsItem>> getLiked(@Query("user_id") String userId);

        @GET("/news_items/{id}/likes_count")
        Call<Integer> getLikesCount(@Path("id") long postId);

        @GET("/news_item_user_likes/search_news_by_user")
        @SuppressLint("SupportAnnotationUsage")
        Call<List<NewsContent.NewsItem>> getLiked(@Query("user_id") String userId,
                                                  @Query("criteria") @UpdateType char sign,
                                                  @Query("date") Timestamp time);
    }

    public static class Callback<L> implements retrofit2.Callback<L> {
        private final Context context;

        public Callback(Context context) {
            this.context = context;
        }

        @Deprecated
        @SuppressWarnings("unchecked")
        @Override
        public void onResponse(Call call, Response response) {
            String url = call.request().url().toString();
            if (response.isSuccessful()) {
                onSuccess(response, url);
            } else {
                onError(ErrorType.UNSUCCESSFUL, String.valueOf(response.code()), url);
            }
        }

        @Deprecated
        @Override
        public void onFailure(Call call, Throwable t) {
            onError(ErrorType.NOT_RECEIVED, t.getMessage(), call.request().url().toString());
        }

        protected void onError(ErrorType errorType, String msg, String url) {
            Logger.i(context, "Request failed with error: " + errorType.name() + " " + msg + ", url: " + url);
        }

        protected void onSuccess(Response<L> response, String url) {
            Logger.i(context, "Response is received with code: " + response.code() + ", url: " + url);
        }
    }
}
