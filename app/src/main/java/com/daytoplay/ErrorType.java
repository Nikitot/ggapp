package com.daytoplay;

import android.content.Context;
import android.support.annotation.Nullable;

public enum ErrorType {
    NO_CONNECTION(R.string.no_connection_error),
    TIME_OUT(R.string.out_of_time),
    NOT_RECEIVED(R.string.not_received_error),
    ERROR(R.string.something_error),
    NO_ONE(R.string.no_one_error),
    YOUTUBE(R.string.youtube_error),
    UNKNOWN(R.string.unknown_error),
    SEARCH(R.string.search_error),
    UNSUCCESSFUL(R.string.bad_code_error),
    NOT_FOUND(R.string.not_found_error),
    WRONG_TYPE(R.string.wrong_type),
    AUTHORISATION_FAILED(R.string.authorization_failed);

    public int textId;

    ErrorType(int textId) {
        this.textId = textId;
    }

    public String toString(@Nullable Context context) {
        return context != null ? context.getResources().getString(textId) : null;
    }
}
