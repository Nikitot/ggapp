package com.daytoplay.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.flaviofaria.kenburnsview.KenBurnsView;
import com.daytoplay.BuildConfig;
import com.daytoplay.Config;
import com.daytoplay.R;
import com.daytoplay.utils.DownloadImageTask;
import com.daytoplay.utils.Logger;
import com.daytoplay.utils.NavigationHelper;
import com.daytoplay.utils.Utils;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.List;

public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    private static final int RC_SIGN_IN = 9001;
    private GoogleApiClient googleApiClient;
    private KenBurnsView backgroundView;
    private MessageHandler messageHandler = new MessageHandler(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hideSystemUi();


        getWindow().getDecorView().setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                if (visibility == View.VISIBLE) {
                    messageHandler.sendEmptyMessageDelayed(MessageHandler.HIDE_SYSTEM_UI, 4000);
                }
            }
        });

        setContentView(R.layout.activity_login);
        backgroundView = (KenBurnsView) findViewById(R.id.background);

        if (BuildConfig.DEBUG) {
            initSpinner();
        }

        messageHandler.sendEmptyMessage(MessageHandler.START_SLIDESHOW);
        findViewById(R.id.sing_in_with_google).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.setSkipSignIn(LoginActivity.this, false);
                signIn();
            }
        });

        findViewById(R.id.skip).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.setSkipSignIn(LoginActivity.this, true);
                NavigationHelper.startActivityNoHistory(LoginActivity.this, FeedActivity.class);
            }
        });

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                GoogleSignInAccount account = result.getSignInAccount();
                firebaseAuthWithGoogle(account);
            } else {
                onLoginFailed("Failed with selected User, status: " + result.getStatus());
            }
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        Config.auth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser user = Config.auth.getCurrentUser();
                            if (user != null) {
                                NavigationHelper.startActivityNoHistory(LoginActivity.this, FeedActivity.class);
                                Logger.i(LoginActivity.this, "uid " + user.getUid() + " is logged with Google");
                                return;
                            }
                        }
                        onLoginFailed("signInWithCredential:failure " + task.getException());
                    }
                });
    }

    private void onLoginFailed(String msg) {
        Logger.i(LoginActivity.this, msg);
        showErrorToast();
    }

    private void initSpinner() {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, Arrays.asList("Production", "Test #1", "Test #2"));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        findViewById(R.id.servers_layout).setVisibility(View.VISIBLE);
        Spinner spinner = (Spinner) findViewById(R.id.servers_spinner);
        spinner.setAdapter(adapter);
        spinner.setSelection(Config.getPortPosition());
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Config.setPortPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }

    private void hideSystemUi() {
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                | View.SYSTEM_UI_FLAG_IMMERSIVE);
    }

    private void showErrorToast() {
        Utils.toast(LoginActivity.this, getString(R.string.authorization_failed));
    }

    public void setBackground(final Bitmap background) {
        final int valX = Config.defaultSize.x / 4;
        final int duration = 150;
        backgroundView.animate()
                .x(-valX)
                .alpha(0f)
                .setDuration(duration)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        backgroundView.setImageBitmap(background);
                        backgroundView.setAlpha(0f);
                        backgroundView.setX(valX);
                        backgroundView.animate()
                                .x(0)
                                .alpha(1f)
                                .setDuration(duration)
                                .setListener(null)
                                .start();
                    }
                })
                .start();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Utils.toast(LoginActivity.this, "Connection failed");
        Logger.e(this, connectionResult.getErrorMessage());
    }

    private static class MessageHandler extends Handler {

        static final int HIDE_SYSTEM_UI = 2;
        private static final int START_SLIDESHOW = 1;
        List<String> urls = Utils.getBackgroundLinks();
        int i;
        private final WeakReference<LoginActivity> activity;

        MessageHandler(LoginActivity view) {
            activity = new WeakReference<>(view);
        }

        @Override
        public void handleMessage(Message msg) {
            LoginActivity activity = this.activity.get();
            if (activity == null) return;
            switch (msg.what) {
                case START_SLIDESHOW:
                    if (i < urls.size()) {
                        if (Utils.isConneted(activity)) {
                            DownloadImageTask downloadImageTask = new DownloadImageTask() {
                                @Override
                                protected void onBitmapLoaded(Bitmap bitmap) {
                                    LoginActivity activity = MessageHandler.this.activity.get();
                                    if (activity == null) return;
                                    if (bitmap != null) {
                                        setBackground(bitmap, activity, false);
                                    } else {
                                        sendMessage(START_SLIDESHOW, 1000);
                                    }
                                }
                            };
                            downloadImageTask.execute(urls.get(i));
                        }
                        sendMessage(START_SLIDESHOW, 15000);
                    } else {
                        removeMessages(START_SLIDESHOW);
                    }
                    break;
                case HIDE_SYSTEM_UI:
                    activity.hideSystemUi();
                    break;
            }
        }

        void sendMessage(int what, int delayMillis) {
            removeMessages(what);
            sendEmptyMessageDelayed(what, delayMillis);
        }

        void setBackground(Bitmap bitmap, @NonNull LoginActivity activity, boolean preloaded) {
            if (!preloaded) {
                activity.setBackground(bitmap);
            }
            i++;
        }
    }
}

