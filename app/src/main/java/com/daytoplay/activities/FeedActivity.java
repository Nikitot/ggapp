package com.daytoplay.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Pair;
import android.view.View;
import android.widget.FrameLayout;

import com.facebook.drawee.generic.RoundingParams;
import com.daytoplay.Config;
import com.daytoplay.R;
import com.daytoplay.adapters.BaseFeedRecyclerAdapter;
import com.daytoplay.fragments.BaseNewsRecyclerFragment;
import com.daytoplay.items.PageInfo;
import com.daytoplay.utils.AnimationHelper;
import com.daytoplay.views.NativeAd;
import com.daytoplay.views.SectionIndicatorView;
import com.daytoplay.views.TitleTextView;
import com.daytoplay.views.TranslateDraweeView9x16;

import java.util.ArrayList;
import java.util.List;

public class FeedActivity extends BaseNewsActivity implements SectionIndicatorView.Listener {

    public static final int INITIAL_PAGE = 0;
    SectionType currentSection;
    private ArrayList<PageInfo> pageInfos;
    private SectionIndicatorView magicIndicator;
    private TitleTextView title;
    private TranslateDraweeView9x16 profileIcon;
    private SectionPagerAdapter adapter;
    private ViewPager viewPager;
    protected NativeAd adView;
    protected FrameLayout initialAdContainer;

    public enum SectionType {
        NEW, LIKED;

        public static int size() {
            return values().length;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View rootView = findViewById(R.id.layout);
        final View searchButton = findViewById(R.id.search_icon);
        profileIcon = (TranslateDraweeView9x16) findViewById(R.id.profile_icon);
        viewPager = (ViewPager) rootView.findViewById(R.id.fragment_pager);
        magicIndicator = (SectionIndicatorView) findViewById(R.id.magic_indicator);
        initialAdContainer = ((FrameLayout) findViewById(R.id.initial_ad_container));
        title = (TitleTextView) findViewById(R.id.title);

        adView = NativeAd.getInstance(this);
        addInitialAd();
        title.setTypeface(Typeface.createFromAsset(this.getAssets(), "RobotoTTF/Roboto-Bold.ttf"));
        pageInfos = createTitlesInfos();
        adapter = new SectionPagerAdapter(getFragmentManager(), createSectionFragments());
        viewPager.setAdapter(adapter);
        magicIndicator.bind(pageInfos, viewPager);
        viewPager.setCurrentItem(INITIAL_PAGE);
        title.setText(pageInfos.get(INITIAL_PAGE).text);

        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                magicIndicator.hopCurrent(viewPager.getCurrentItem());
            }
        });

        profileIcon.setImageResource(R.drawable.ic_account_circle_white_24dp);

        if (Config.hasUser()) {
            Uri photoUrl = Config.auth.getCurrentUser().getPhotoUrl();
            if (photoUrl != null) {
                profileIcon.setImageURI(photoUrl);
                RoundingParams roundingParams = RoundingParams.fromCornersRadius(5f);
                roundingParams.setRoundAsCircle(true);
                profileIcon.getHierarchy().setRoundingParams(roundingParams);
            }
        }

        profileIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showMenu();
            }
        });

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            @SuppressLint("NewApi")
            public void onClick(View v) {
                Intent intent = new Intent(FeedActivity.this, SearchActivity.class);
                if (AnimationHelper.isTransitionAnimationAvailable(false)) {
                    ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(FeedActivity.this,
                            Pair.create(searchButton, searchButton.getTransitionName()));
                    startActivity(intent, options.toBundle());
                } else {
                    startActivity(intent);
                }
            }
        });

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_feed;
    }

    private void addInitialAd() {
        initialAdContainer.addView(adView.getView());
    }

    public void removeInitialAd() {
        initialAdContainer.removeView(adView.getView());
    }

    public NativeAd getAd() {
        return adView;
    }


    @NonNull
    private ArrayList<Fragment> createSectionFragments() {
        ArrayList<Fragment> data = new ArrayList<>();
        for (SectionType type : SectionType.values()) {
            data.add(BaseNewsRecyclerFragment.newInstance(type));
        }
        return data;
    }

    @NonNull
    private ArrayList<PageInfo> createTitlesInfos() {
        ArrayList<PageInfo> pageInfos = new ArrayList<>();
        for (SectionType type : SectionType.values()) {
            PageInfo info = getTitleInfo(type);
            pageInfos.add(info);
        }

        return pageInfos;
    }

    @NonNull
    private PageInfo getTitleInfo(SectionType type) {
        return new PageInfo(this, type);
    }


    @SuppressLint("NewApi")
    private void showMenu() {
        Intent intent = new Intent(this, MenuActivity.class);
        if (AnimationHelper.isTransitionAnimationAvailable(true) && Config.hasUser()) {
            ActivityOptions options = ActivityOptions
                    .makeSceneTransitionAnimation(this, profileIcon, profileIcon.getTransitionName());
            startActivity(intent, options.toBundle());
        } else {
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        }
    }

    private String getTitleSectionType(SectionType type) {
        return getTitleInfo(type).text;
    }

    @Override
    protected BaseNewsRecyclerFragment getCurrentNewsFragment() {
        return (BaseNewsRecyclerFragment) adapter.getItem(viewPager.getCurrentItem());
    }

    private Fragment getSectionFragment(SectionType type) {
        return adapter.getItem(type.ordinal());
    }

    @Override
    public void onSectionSelected(final SectionType newType) {
        if (newType != currentSection) {
            changeAnimatedTitle(newType);
            currentSection = newType;
            hideSnackBar();
        }
        if (newType == SectionType.LIKED) {
            ((BaseNewsRecyclerFragment) getSectionFragment(SectionType.LIKED)).updateContent(true);
        }
    }

    public void changeAnimatedTitle(final SectionType newType) {
        if (currentSection != null) {
            int x = 100;
            if (newType.ordinal() > currentSection.ordinal()) {
                x = -x;
            }
            final int valX = x;
            title.animate()
                    .x(x)
                    .alpha(0f)
                    .setDuration(100)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            title.setText(getTitleSectionType(newType));
                            title.setAlpha(0f);
                            title.setX(-valX);
                            title.animate()
                                    .x(0)
                                    .alpha(1f)
                                    .setDuration(100)
                                    .setListener(null)
                                    .start();
                        }
                    })
                    .start();
        }
    }

    @Override
    public void onLikedChanged(boolean like, long newsId, int count) {
        for (SectionType type : SectionType.values()) {
            BaseNewsRecyclerFragment sectionFragment = (BaseNewsRecyclerFragment) getSectionFragment(type);
            BaseFeedRecyclerAdapter adapter = sectionFragment.getAdapter();
            if (adapter != null) {
                adapter.updateItemLike(like, newsId, count);
            }
        }
    }

    private class SectionPagerAdapter extends FragmentStatePagerAdapter {

        private final List<Fragment> data;

        public SectionPagerAdapter(FragmentManager fm, List<Fragment> data) {
            super(fm);
            this.data = data;
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Fragment getItem(int position) {
            if (position < data.size()) {
                return data.get(position);
            }
            return null;
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return pageInfos.get(position).text;
        }
    }
}
