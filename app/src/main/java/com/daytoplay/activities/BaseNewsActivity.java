package com.daytoplay.activities;

import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Pair;
import android.view.View;

import com.gg.api.Channel;
import com.daytoplay.R;
import com.daytoplay.fragments.BaseNewsRecyclerFragment;
import com.daytoplay.items.NewsContent;
import com.daytoplay.utils.AnimationHelper;
import com.daytoplay.utils.Logger;
import com.daytoplay.utils.NavigationHelper;
import com.daytoplay.views.FeedItemHolder;
import com.daytoplay.views.LikeView;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.lang.ref.WeakReference;

public abstract class BaseNewsActivity extends AppCompatActivity implements BaseNewsRecyclerFragment.RecyclerListener, LikeView.LikeListener {

    protected Toolbar toolbar;
    protected Snackbar loadingSnackBar;
    protected FeedActivity.MessageHandler messageHandler = new FeedActivity.MessageHandler(this);
    protected ConnectivityManager cm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        loadingSnackBar = Snackbar.make(getWindow().getDecorView().getRootView(), "loading more...", Snackbar.LENGTH_LONG);
        cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    protected abstract int getLayoutId();


    public boolean isConnected() {
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    protected void setHomeButtonVisibility(boolean visibility) {
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setHomeButtonEnabled(visibility);
            supportActionBar.setDisplayHomeAsUpEnabled(visibility);
        }
    }


    @Override
    @SuppressLint("NewApi")
    public void onRecyclerItemClicked(final FeedItemHolder viewHolder) {
        Logger.s(this, FirebaseAnalytics.Event.SELECT_CONTENT, viewHolder.getParams());
        Intent intent = new Intent(this, PostActivity.class);
        intent.putExtra(PostActivity.ITEM, viewHolder.item);
        intent.putExtra(PostActivity.TITLE_WIDTH, viewHolder.title.getWidth());

        if (AnimationHelper.isTransitionAnimationAvailable(true)) {
            View channelTextView = viewHolder.channelTextView;
            View channelImageView = viewHolder.channelImageView;

            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(this,
                    Pair.create(((View) viewHolder.thumbnail), "thumbnail"),
                    Pair.create(((View) viewHolder.title), "title"),
                    Pair.create(((View) viewHolder.dateView), "date"),
                    Pair.create(channelTextView, channelTextView.getVisibility() == View.VISIBLE ? "rootText" : ""),
                    Pair.create(channelImageView, channelImageView.getVisibility() == View.VISIBLE ? "rootImage" : ""));

            if (viewHolder.item.channel != null) {
                intent.putExtra(NewsContent.ROOT_LINK, viewHolder.item.channel.url);
            }
            startActivity(intent, options.toBundle());
            overridePendingTransition(0, 0);
            restoreSelectedHolder(viewHolder.getAdapterPosition(), getCurrentNewsFragment());
        } else {
            startActivity(intent);
        }
    }

    protected void restoreSelectedHolder(int holderPosition, BaseNewsRecyclerFragment fragment) {
        messageHandler.removeMessages(FeedActivity.MessageHandler.RESTORE_HOLDER);
        messageHandler.sendMessageDelayed(Message.obtain(messageHandler,
                FeedActivity.MessageHandler.RESTORE_HOLDER,
                0,
                holderPosition,
                fragment), PostActivity.TRANSITION_DELAY);
    }

    @Override
    public void onShareClicked(String link) {
        NavigationHelper.shareLink(this, link);
    }

    @Override
    public void onChannelClicked(Channel channel) {
        Intent intent = new Intent(this, ChannelActivity.class);
        intent.putExtra(ChannelActivity.CHANNEL, channel);
        startActivity(intent);
    }

    @Override
    public void onUpdateStarted(boolean top) {
        if (!top && !loadingSnackBar.isShown())
            loadingSnackBar.show();
    }

    @Override
    public void onUpdateFinished() {
        hideSnackBar();
    }

    protected void hideSnackBar() {
        if (loadingSnackBar.isShown())
            loadingSnackBar.dismiss();
    }

    @Nullable
    protected abstract BaseNewsRecyclerFragment getCurrentNewsFragment();

    protected static class MessageHandler extends Handler {

        protected static final int RESTORE_HOLDER = 1;
        private final WeakReference<BaseNewsActivity> activity;

        MessageHandler(BaseNewsActivity view) {
            activity = new WeakReference<>(view);
        }

        @Override
        public void handleMessage(Message msg) {
            BaseNewsActivity activity = this.activity.get();
            if (activity == null) {
                return;
            }
            switch (msg.what) {
                case (RESTORE_HOLDER):
                    BaseNewsRecyclerFragment baseNewsRecyclerFragment = (BaseNewsRecyclerFragment) msg.obj;
                    if (baseNewsRecyclerFragment != null) {
                        baseNewsRecyclerFragment.notifyItem(msg.arg2);
                    }
                    break;
            }
        }
    }


    @Override
    public abstract void onLikedChanged(boolean like, long newsId, int count);

}
