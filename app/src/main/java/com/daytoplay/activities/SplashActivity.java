package com.daytoplay.activities;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.daytoplay.Config;
import com.daytoplay.R;
import com.daytoplay.utils.NavigationHelper;
import com.daytoplay.utils.Utils;
import java.lang.ref.WeakReference;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class SplashActivity extends AppCompatActivity {
    MessageHandler messageHandler = new MessageHandler(this);
    private View errorView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        errorView = findViewById(R.id.error);
        if (!start()) {
            messageHandler.sendEmptyMessageDelayed(MessageHandler.CHECK_NETWORK, 2000);
        }
    }

    private boolean start() {
        if (Utils.isConneted(this)) {
            Config.get().fetchRemote(this);
            errorView.setVisibility(View.INVISIBLE);
            if (Config.hasUser()) {
                NavigationHelper.startActivityNoHistory(this, FeedActivity.class);
            } else {
                if (Utils.isSignInSkipped(this)) {
                    NavigationHelper.startActivityNoHistory(this, FeedActivity.class);
                } else {
                    NavigationHelper.startActivityNoHistory(this, LoginActivity.class);
                }
            }
            return true;
        }
        errorView.setVisibility(View.VISIBLE);
        return false;
    }

    private static class MessageHandler extends Handler {

        private static final int CHECK_NETWORK = 1;
        private final WeakReference<SplashActivity> activity;

        MessageHandler(SplashActivity view) {
            activity = new WeakReference<>(view);
        }

        @Override
        public void handleMessage(Message msg) {
            SplashActivity activity = this.activity.get();
            if (activity == null) return;
            switch (msg.what) {
                case CHECK_NETWORK:
                    if (!activity.start()) {
                        removeMessages(CHECK_NETWORK);
                        sendEmptyMessageDelayed(CHECK_NETWORK, 2000);
                    }
                    break;
            }
        }
    }
}
