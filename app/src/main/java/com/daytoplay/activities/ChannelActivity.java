package com.daytoplay.activities;

import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.graphics.Palette;
import android.view.MenuItem;
import android.view.View;

import com.facebook.common.executors.CallerThreadExecutor;
import com.facebook.common.references.CloseableReference;
import com.facebook.datasource.DataSource;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.core.ImagePipeline;
import com.facebook.imagepipeline.datasource.BaseBitmapDataSubscriber;
import com.facebook.imagepipeline.image.CloseableImage;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.gg.api.Channel;
import com.daytoplay.R;
import com.daytoplay.fragments.BaseNewsRecyclerFragment;
import com.daytoplay.fragments.ChannelNewsRecyclerFragment;
import com.daytoplay.views.TranslateDraweeView;

public class ChannelActivity extends BaseNewsActivity {

    public static final String CHANNEL = "channel";
    private static final String TAG_FRAGMENT = "channel_fragment";
    private int darkColor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        final TranslateDraweeView channelThumbnail = (TranslateDraweeView) findViewById(R.id.thumbnail);
        setHomeButtonVisibility(true);
        Channel channel = (Channel) getIntent().getSerializableExtra(CHANNEL);
        fab.setVisibility(View.GONE);
        if (channel == null) {
            finish();
        } else {
            ChannelNewsRecyclerFragment channelNewsRecyclerFragment = ChannelNewsRecyclerFragment.newInstance(channel);
            getFragmentManager().beginTransaction()
                    .add(R.id.container, channelNewsRecyclerFragment, TAG_FRAGMENT)
                    .commit();

            ActionBar supportActionBar = getSupportActionBar();
            if (supportActionBar != null) {
                supportActionBar.setTitle(channel.name);
            }
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    fab.hide();
                }
            });

            final Uri uri = Uri.parse(channel.getLogo());


            ImageRequest imageRequest = ImageRequestBuilder
                    .newBuilderWithSource(uri)
                    .setProgressiveRenderingEnabled(true)
                    .build();

            ImagePipeline imagePipeline = Fresco.getImagePipeline();
            final DataSource<CloseableReference<CloseableImage>> dataSource = imagePipeline
                    .fetchDecodedImage(imageRequest, this);
            final CollapsingToolbarLayout toolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
            darkColor = getResources().getColor(R.color.color_bars);

            toolbarLayout.setBackgroundColor(darkColor);
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    initAppBar(dataSource, channelThumbnail, uri, fab, toolbarLayout);
                }
            });
        }
    }

    private void initAppBar(DataSource<CloseableReference<CloseableImage>> dataSource, final TranslateDraweeView channelThumbnail, final Uri uri, final FloatingActionButton fab, final CollapsingToolbarLayout toolbarLayout) {
        dataSource.subscribe(new BaseBitmapDataSubscriber() {
            @Override
            public void onNewResultImpl(@Nullable Bitmap b) {
                channelThumbnail.setImageURI(uri);
                if (b != null && !b.isRecycled()) {
                    Palette.from(Bitmap.createScaledBitmap(b, b.getWidth() / 5, b.getHeight() / 5, false)).generate(new Palette.PaletteAsyncListener() {
                        @Override
                        public void onGenerated(Palette palette) {
                            int defaultColor = darkColor;
                            int vibrant = palette.getVibrantColor(defaultColor);
                            int vibrantLight = palette.getLightVibrantColor(defaultColor);
                            int vibrantDark = palette.getDarkVibrantColor(defaultColor);
                            int muted = palette.getMutedColor(defaultColor);

                            fab.show();
                            if (vibrant != defaultColor) {
                                darkColor = vibrant;
                            } else if (muted != defaultColor) {
                                darkColor = muted;
                            } else if (vibrantDark != defaultColor) {
                                darkColor = vibrantDark;
                            } else if (vibrantLight != defaultColor) {
                                darkColor = vibrantLight;
                            } else {
                                return;
                            }
                            fab.setBackgroundTintList(ColorStateList.valueOf(darkColor));
                            toolbarLayout.setContentScrimColor(darkColor);
                        }
                    });
                }
            }

            @Override
            public void onFailureImpl(DataSource dataSource) {
            }

        }, CallerThreadExecutor.getInstance());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_channel;
    }

    @Nullable
    @Override
    protected BaseNewsRecyclerFragment getCurrentNewsFragment() {
        return (BaseNewsRecyclerFragment) getFragmentManager().findFragmentByTag(TAG_FRAGMENT);
    }

    @Override
    public void onLikedChanged(boolean like, long newsId, int count) {

    }
}
