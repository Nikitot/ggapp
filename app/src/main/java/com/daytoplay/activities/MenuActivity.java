package com.daytoplay.activities;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.facebook.drawee.generic.RoundingParams;
import com.daytoplay.Config;
import com.daytoplay.R;
import com.daytoplay.utils.NavigationHelper;
import com.daytoplay.views.TranslateDraweeView9x16;
import com.google.firebase.auth.FirebaseUser;

public class MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        View rootView = findViewById(R.id.activity_profile);
        TranslateDraweeView9x16 photoView = (TranslateDraweeView9x16) findViewById(R.id.profile_icon);
        TextView nameView = (TextView) rootView.findViewById(R.id.profile_name);
        TextView emailView = (TextView) rootView.findViewById(R.id.email);

        TextView signInView = (TextView) rootView.findViewById(R.id.sign_in);
        final TextView signOutView = (TextView) rootView.findViewById(R.id.sign_out);

        Uri photo;
        String name;
        String email;

        if (Config.hasUser()) {
            FirebaseUser user = Config.auth.getCurrentUser();
            photo = user.getPhotoUrl();
            name = user.getDisplayName();
            email = user.getEmail();
            if (photo != null) {
                photoView.setImageURI(photo);
                RoundingParams roundingParams = RoundingParams.fromCornersRadius(5f);
                roundingParams.setRoundAsCircle(true);
                photoView.getHierarchy().setRoundingParams(roundingParams);
            } else {
                photoView.setImageResource(R.drawable.ic_account_circle_white_24dp);
            }
            setText(nameView, name);
            setText(emailView, email);
            signInView.setVisibility(View.GONE);
        } else {
            rootView.findViewById(R.id.info_layout).setVisibility(View.GONE);
            signOutView.setVisibility(View.GONE);
        }

        signOutView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Config.auth.signOut();
                NavigationHelper.startActivityClearTask(MenuActivity.this, LoginActivity.class);
            }
        });

        signInView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavigationHelper.startActivityNoHistory(MenuActivity.this, LoginActivity.class);
            }
        });

        rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    private void setText(TextView nameView, String name) {
        if (name != null) {
            nameView.setText(name);
        } else {
            nameView.setVisibility(View.GONE);
        }
    }
}
