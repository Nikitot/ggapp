package com.daytoplay.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gg.api.Channel;
import com.gg.api.PostItem;
import com.gg.api.PostPartItem;
import com.daytoplay.Config;
import com.daytoplay.Connector;
import com.daytoplay.ErrorType;
import com.daytoplay.R;
import com.daytoplay.fragments.PostRecyclerFragment;
import com.daytoplay.items.NewsContent;
import com.daytoplay.utils.AnimationHelper;
import com.daytoplay.utils.Logger;
import com.daytoplay.utils.NavigationHelper;
import com.daytoplay.utils.Utils;
import com.daytoplay.views.LikeView;
import com.daytoplay.views.TitleTextView;
import com.daytoplay.views.TranslateDraweeView9x16;
import com.daytoplay.views.YouTubeCustomToolbar;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.stfalcon.frescoimageviewer.ImageViewer;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

public class PostActivity extends YouTubeBaseActivity
        implements PostRecyclerFragment.OnContentPartsClickListener,
        ImageViewer.FrescoImageViewerListener {

    public static final int TRANSITION_DELAY = 500;
    public static final String ITEM = "ITEM";
    public static final String TITLE_WIDTH = "TITLE_WIDTH";
    public static final int requestCode = 1;
    @Nullable
    private NewsContent.NewsItem newsItem;
    private YouTubeCustomToolbar toolbar;
    private View rootLayoutGradient;
    private long id;
    private RelativeLayout contentLayout;
    private int titleWidth = FrameLayout.LayoutParams.MATCH_PARENT;
    private MessageHandler messageHandler = new MessageHandler(this);
    private View progressBar;
    private int toolbarThreshold = 300;
    private TextView channelTitleView;
    private TranslateDraweeView9x16 channelImageView;
    private View headerLayout;
    private View contentFragmentContainer;
    private LikeView likeView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);
        headerLayout = findViewById(R.id.header_post);
        toolbar = (YouTubeCustomToolbar) headerLayout.findViewById(R.id.toolbar);
        rootLayoutGradient = headerLayout.findViewById(R.id.root_layout_gradient);
        final TranslateDraweeView9x16 thumbnail = (TranslateDraweeView9x16) headerLayout.findViewById(R.id.thumbnail);
        channelImageView = (TranslateDraweeView9x16) headerLayout.findViewById(R.id.channel_image);
        Utils.roundCrop(channelImageView);
        channelTitleView = (TextView) headerLayout.findViewById(R.id.root_text);
        TextView dateView = (TextView) headerLayout.findViewById(R.id.date);
        likeView = (LikeView) findViewById(R.id.like);
        TitleTextView title = (TitleTextView) findViewById(R.id.post_title);
        TextView viewsCount = (TextView) findViewById(R.id.views_count);
        contentLayout = (RelativeLayout) findViewById(R.id.content_layout);
        contentFragmentContainer = findViewById(R.id.fragment_container_post);
        progressBar = findViewById(R.id.progress);
        View share = findViewById(R.id.share);
        NestedScrollView mainScroll = (NestedScrollView) findViewById(R.id.scroll);
        final View backgroundContent = findViewById(R.id.background_content);
        backgroundContent.getLayoutParams().height = getContentLayoutMinSize();
        thumbnail.post(new Runnable() {
            @Override
            public void run() {
                int height = thumbnail.getHeight();
                toolbarThreshold = Math.round(height * 1.1f);
            }
        });

        final int toolbarHeight = getResources().getDimensionPixelSize(R.dimen.toolbar_height_with_status);

        mainScroll.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                int delta = scrollY - oldScrollY;
                if (delta < 0) {
                    toolbar.setY(Math.min(toolbar.getY() - delta, 0));                     //mainScroll up
                } else {
                    toolbar.setY(-Math.min(delta - toolbar.getY(), toolbarHeight));        //mainScroll down
                }
                setVisibilityTitle(scrollY > toolbarThreshold);
            }
        });

        toolbar.findViewById(R.id.back).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onBackPressed();
                    }
                });

        Bundle extras = getIntent().getExtras();


        newsItem = extras.getParcelable(ITEM);
        titleWidth = extras.getInt(TITLE_WIDTH);
        contentFragmentContainer.setVisibility(View.GONE);
        toolbar.setVisibility(View.GONE);
        rootLayoutGradient.setVisibility(View.GONE);
        messageHandler.sendEmptyMessageDelayed(MessageHandler.LOAD_CONTENT, TRANSITION_DELAY);

        title.setWidth(titleWidth);

        if (newsItem != null) {
            String thumbnailUrl = newsItem.thumbnail_url;
            if (thumbnailUrl != null) {
                try {
                    thumbnail.setImageURI(Uri.parse(thumbnailUrl));
                } catch (NullPointerException ex) {
                    Logger.e(this, ex.getMessage());
                }
            }
            title.setText(newsItem.title);
            toolbar.setTitle(newsItem.title);

            share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    NavigationHelper.shareLink(PostActivity.this, newsItem.link);
                }
            });

            if (newsItem.channel != null) {
                initChannelInfo();
            } else {
                Connector.createService().getChannel(newsItem.news_item_type).enqueue(new Connector.Callback<Channel>(this) {
                    @Override
                    protected void onSuccess(Response<Channel> response, String url) {
                        super.onSuccess(response, url);
                        newsItem.channel = response.body();
                        initChannelInfo();
                    }
                });
            }
            dateView.setText(newsItem.getCreatedDate());
            likeView.init(newsItem, null);
            id = newsItem.id;
            viewsCount.setText(Utils.formatCount(newsItem.views_count));
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        likeView.register();
    }

    private int getContentLayoutMinSize() {
        return Config.defaultSize.y +
                Utils.getNavigationBarHeight(this) - Math.round(Config.defaultSize.x * Utils.ratio9x16) + 5;
    }

    private void initChannelInfo() {
        if (newsItem != null && newsItem.channel != null) {
            channelImageView.setImageURI(newsItem.channel.logo);
            channelTitleView.setText(newsItem.channel.name);
        }
    }

    private void setVisibilityTitle(boolean visibility) {
        if (visibility) {
            toolbar.showBackground();
        } else {
            toolbar.hideTitle();
        }
    }

    @Override
    public void onBackPressed() {
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            setPortrait();
        } else {
            hideHeaderGradients();
            if (AnimationHelper.isTransitionAnimationAvailable(true)) {
                startFinishAnimation();
            } else {
                super.onBackPressed();
            }
        }
    }

    private void onTransitionAnimationComplete() {
        showHeaderGradients();
        progressBar.setVisibility(View.VISIBLE);
        Connector.createService().getPostItem(id).enqueue(new Connector.Callback<List<PostPartItem>>(this) {

            @Override
            protected void onSuccess(Response<List<PostPartItem>> response, String url) {
                super.onSuccess(response, url);
                FragmentManager fm = getFragmentManager();
                if (fm.findFragmentByTag(PostRecyclerFragment.TAG) == null) {
                    List<PostPartItem> items = response.body();
                    if (items != null && items.size() > 0) {
                        PostItem content = new PostItem((ArrayList<PostPartItem>) items);
                        if (!isFinishing()) {
                            fm.beginTransaction()
                                    .replace(R.id.fragment_container_post, PostRecyclerFragment.newInstance(content), PostRecyclerFragment.TAG)
                                    .commitAllowingStateLoss();
                        }
                        if (contentFragmentContainer.getVisibility() == View.GONE) {
                            AnimationHelper.fadeShow(contentFragmentContainer, 600);
                        }
                        progressBar.setVisibility(View.GONE);
                    } else {
                        PostActivity.this.onError(ErrorType.NO_ONE);
                    }
                }
            }

            @Override
            protected void onError(ErrorType errorType, String msg, String url) {
                super.onError(errorType, msg, url);
                PostActivity.this.onError(ErrorType.NOT_RECEIVED);
            }
        });
    }

    public void onError(ErrorType errorType) {
        // TODO: 09.05.16 show error
    }

    private void showHeaderGradients() {
        AnimationHelper.fadeShow(toolbar, 600);
        AnimationHelper.fadeShow(rootLayoutGradient, 600);
    }

    private void hideHeaderGradients() {
        PostRecyclerFragment postRecyclerFragment = (PostRecyclerFragment) getFragmentManager()
                .findFragmentByTag(PostRecyclerFragment.TAG);
        if (postRecyclerFragment != null) {
            postRecyclerFragment.removeVideoContent(false);
        }
        AnimationHelper.fadeHide(toolbar, 600);
        AnimationHelper.fadeHide(rootLayoutGradient, 600);
    }

    @SuppressLint("NewApi")
    @Override
    public void onPhotoClicked(View view, ArrayList<String> list, int position) {
        ImageViewer viewer = new ImageViewer.Builder(this, list)
                .setStartPosition(position)
                .setBackgroundColor(Color.BLACK)
                .show();
        PostRecyclerFragment postRecyclerFragment = (PostRecyclerFragment) getFragmentManager()
                .findFragmentByTag(PostRecyclerFragment.TAG);
        if (postRecyclerFragment != null) {
            postRecyclerFragment.removeVideoContent(true);
        }
        viewer.setListener(this);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
    }

    @Nullable
    public Object getRootLink() {
        Intent intent = getIntent();
        return intent != null ? intent.getStringExtra(NewsContent.ROOT_LINK) : null;
    }

    public void startFinishAnimation() {
        int value = headerLayout.getHeight();
        int duration = 300;
        headerLayout.animate()
                .translationY(-value)
                .setDuration(duration)
                .setInterpolator(new DecelerateInterpolator())
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        finish();
                        overridePendingTransition(0, 0);
                    }
                }).start();
        contentLayout.animate()
                .translationY(value / 2)
                .alpha(0f)
                .setDuration(duration)
                .start();
    }

    @Override
    public void onViewerClose() {
        setPortrait();
    }

    @Override
    public void onViewerBackPressed() {
        setPortrait();
    }

    public void setPortrait() {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @Override
    protected void onStop() {
        super.onStop();
        likeView.unregister();
    }

    private static class MessageHandler extends Handler {

        public static final int LOAD_CONTENT = 0;
        private final WeakReference<PostActivity> activity;

        MessageHandler(PostActivity view) {
            activity = new WeakReference<>(view);
        }

        @Override
        public void handleMessage(Message msg) {
            PostActivity activity = this.activity.get();
            if (activity == null) {
                return;
            }
            switch (msg.what) {
                case (LOAD_CONTENT):
                    activity.onTransitionAnimationComplete();
                    break;
            }
        }
    }
}
