package com.daytoplay.activities;

import android.app.FragmentManager;
import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.CursorAdapter;
import android.widget.SearchView;
import android.widget.TextView;

import com.daytoplay.Connector;
import com.daytoplay.R;
import com.daytoplay.adapters.BaseFeedRecyclerAdapter;
import com.daytoplay.fragments.BaseNewsRecyclerFragment;
import com.daytoplay.fragments.SearchRecyclerFragment;
import com.daytoplay.utils.Logger;

import java.util.List;

import retrofit2.Response;

public class SearchActivity extends BaseNewsActivity {

    private SearchView searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHomeButtonVisibility(true);
        searchView = (SearchView) findViewById(R.id.search_view);
        searchView.onActionViewExpanded();

        int autoCompleteTextViewID = getResources().getIdentifier("android:id/search_src_text", null, null);
        AutoCompleteTextView searchAutoCompleteTextView = (AutoCompleteTextView) searchView.findViewById(autoCompleteTextViewID);
        searchAutoCompleteTextView.setThreshold(1);

        searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {

            @Override
            public boolean onSuggestionSelect(int position) {
                return false;
            }

            @Override
            public boolean onSuggestionClick(int position) {
                Cursor cursor = searchView.getSuggestionsAdapter().getCursor();
                cursor.moveToPosition(position);
                String query = cursor.getString(1) + " ";
                searchView.setQuery(query, true);
                return false;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                setSearch(query);
                Logger.i(SearchActivity.this, "search " + query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                loadSuggestion(newText);
                Logger.i(SearchActivity.this, "suggestion " + newText);
                return false;
            }
        });

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                onBackPressed();
                return false;
            }
        });
    }

    protected int getLayoutId() {
        return R.layout.activity_search;
    }

    @Override
    protected BaseNewsRecyclerFragment getCurrentNewsFragment() {
        FragmentManager fm = getFragmentManager();
        return (BaseNewsRecyclerFragment) fm.findFragmentByTag(getResources().getString(R.string.search_tag));
    }

    @Override
    public void onLikedChanged(boolean like, long newsId, int count) {
        BaseNewsRecyclerFragment currentNewsFragment = getCurrentNewsFragment();
        BaseFeedRecyclerAdapter adapter;
        if (currentNewsFragment != null) {
            adapter = currentNewsFragment.getAdapter();
            if (adapter != null) {
                adapter.updateItemLike(like, newsId, count);
            }
        }
    }


    private void setSearch(String query) {
        Logger.i(SearchActivity.this, "set search " + query);
        SearchRecyclerFragment currentNewsFragment = (SearchRecyclerFragment) getCurrentNewsFragment();
        if (currentNewsFragment != null) {
            currentNewsFragment.setQuery(query);
        }
    }


    private void loadSuggestion(String query) {
        Connector.createService()
                .getSuggestion(query).enqueue(new Connector.Callback<List<String>>(this) {
            @Override
            protected void onSuccess(Response<List<String>> response, String url) {
                super.onSuccess(response, url);
                SuggestionsAdapter suggestionsAdapter = (SuggestionsAdapter) searchView.getSuggestionsAdapter();
                List<String> body = response.body();
                if (suggestionsAdapter != null) {
                    suggestionsAdapter.changeCursor(SuggestionsAdapter.createCursor(body));
                } else {
                    SuggestionsAdapter adapter = new SuggestionsAdapter(SearchActivity.this,
                            SuggestionsAdapter.createCursor(body));
                    searchView.setSuggestionsAdapter(adapter);
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    private static class SuggestionsAdapter extends CursorAdapter {
        public static final String ID = "_id";
        public static final String TEXT = "TEXT";

        public SuggestionsAdapter(Context context, Cursor cursor) {
            super(context, cursor, 0);
        }

        public static Cursor createCursor(List<String> suggestions) {
            String[] columns = new String[]{SuggestionsAdapter.ID, SuggestionsAdapter.TEXT};
            MatrixCursor cursor = new MatrixCursor(columns);
            for (int i = 0; i < suggestions.size(); i++) {
                String suggestion = suggestions.get(i);
                cursor.addRow(new Object[]{i, suggestion});
            }
            return cursor;
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from(context);
            return inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            TextView tv = (TextView) view;
            final int textIndex = cursor.getColumnIndex(TEXT);
            if (textIndex >= 0)
                tv.setText(cursor.getString(textIndex));
        }
    }
}
