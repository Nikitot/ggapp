package com.daytoplay.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.gg.api.Channel;
import com.daytoplay.Config;
import com.daytoplay.R;
import com.daytoplay.fragments.BaseNewsRecyclerFragment;
import com.daytoplay.items.NewsContent;

import java.util.List;

public class NewFeedRecyclerAdapter extends BaseFeedRecyclerAdapter {

    private final List<Channel> channels;

    public NewFeedRecyclerAdapter(@NonNull BaseNewsRecyclerFragment fragment, List<NewsContent.NewsItem> items, List<Channel> channels, BaseNewsRecyclerFragment.RecyclerListener listener) {
        super(fragment, items, listener);
        this.channels = channels;
    }

    @Override
    protected int getHeaderLayoutId() {
        return R.layout.item_channels;
    }

    @Override
    public RecyclerView.ViewHolder createHeaderItemViewHolder(View view) {
        return new ChannelsItemHolder(view);
    }

    @Override
    protected void bindHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {
        ChannelsItemHolder channelHolder = (ChannelsItemHolder) holder;
        ChannelItemRecyclerAdapter adapter = new ChannelItemRecyclerAdapter(channels, listener);
        channelHolder.channelsRecyclerView.setAdapter(adapter);
        LinearLayoutManager layout = new LinearLayoutManager(fragment.getActivity(), LinearLayoutManager.HORIZONTAL, false);
        channelHolder.channelsRecyclerView.setLayoutManager(layout);
    }

    public class ChannelsItemHolder extends RecyclerView.ViewHolder {
        public final View view;
        public final RecyclerView channelsRecyclerView;

        public ChannelsItemHolder(View view) {
            super(view);
            this.view = view;
            channelsRecyclerView = (RecyclerView) view.findViewById(R.id.channels_recycler);
        }
    }

    public boolean isAdEnabled() {
        return Config.isShowAd();
    }
}
