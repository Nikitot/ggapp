package com.daytoplay.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.daytoplay.R;
import com.daytoplay.activities.FeedActivity;
import com.daytoplay.items.PageInfo;

import java.util.ArrayList;

public class TextViewPagerAdapter extends PagerAdapter {

    private final Context context;
    private final LayoutInflater inflater;
    private ArrayList<PageInfo> data;

    public TextViewPagerAdapter(Context context, ArrayList<PageInfo> data) {
        this.context = context;
        this.data = data;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return FeedActivity.SectionType.size();
    }


    public Object instantiateItem(ViewGroup collection, int position) {
        TextView view = (TextView) inflater.inflate(R.layout.view_text, null);
        collection.addView(view);
        view.setTypeface(Typeface.createFromAsset(context.getAssets(), "RobotoTTF/Roboto-Bold.ttf"));
        view.setText(data.get(position).text);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
}
