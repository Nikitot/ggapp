package com.daytoplay.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gg.api.Channel;
import com.daytoplay.R;
import com.daytoplay.fragments.BaseNewsRecyclerFragment;
import com.daytoplay.views.ChannelItemHolder;

import java.util.ArrayList;
import java.util.List;

public class ChannelItemRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int CHANNEL = 0;
    private static final int DIVIDER = 1;
    private final BaseNewsRecyclerFragment.RecyclerListener listener;
    private final ArrayList<Channel> channels = new ArrayList<>();
    private final ArrayList<Channel> subscriptions = new ArrayList<>();

    public ChannelItemRecyclerAdapter(@NonNull List<Channel> channels, BaseNewsRecyclerFragment.RecyclerListener listener) {
        this.listener = listener;
        subscriptions.add(channels.get(0));
        subscriptions.add(channels.get(1));

        for (Channel subscription : subscriptions) {
            for (int i = 0; i < channels.size(); i++) {
                Channel channel = channels.get(i);
                if (channel.id == subscription.id) {
                    channels.remove(i);
                    break;
                }
            }
        }

        this.channels.addAll(channels);
    }

    @Override
    public int getItemViewType(int position) {
        if (subscriptions.size() != 0 && position == subscriptions.size()) {
            return DIVIDER;
        } else {
            return CHANNEL;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case DIVIDER:
                return new RecyclerView.ViewHolder((LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_channel_devider, parent, false))) {
                };
            default:
                return new ChannelItemHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_channel, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        int viewType = holder.getItemViewType();
        switch (viewType) {
            case DIVIDER:
                break;
            default:
                final Channel channel;
                if (isSubscription(position)) {
                    channel = subscriptions.get(position);
                } else {
                    channel = channels.get(position - getSubscriptionsCountWithSeparator());
                }
                if (listener != null) {
                    ChannelItemHolder h = (ChannelItemHolder) holder;
                    h.title.setText(channel.getName());
                    h.logo.setImageURI(channel.getLogo());
                    h.logo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            listener.onChannelClicked(channel);
                        }
                    });
                }
        }

    }

    private boolean isSubscription(int position) {
        return position < subscriptions.size();
    }

    @Override
    public int getItemCount() {
        return channels.size() + getSubscriptionsCountWithSeparator();
    }

    private int getSubscriptionsCountWithSeparator() {
        return subscriptions.size() + (subscriptions.size() != 0 ? 1 : 0);
    }
}