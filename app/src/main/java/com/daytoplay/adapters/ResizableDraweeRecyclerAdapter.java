package com.daytoplay.adapters;

import android.graphics.drawable.Animatable;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.drawable.ScalingUtils;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.imagepipeline.image.ImageInfo;
import com.daytoplay.R;
import com.daytoplay.fragments.PostRecyclerFragment;
import com.daytoplay.views.DraweeViewHolder;

import java.util.ArrayList;

public class ResizableDraweeRecyclerAdapter extends RecyclerView.Adapter<DraweeViewHolder> {
    private final PostRecyclerFragment.OnContentPartsClickListener listener;
    private ArrayList<String> photoLinks;

    public ResizableDraweeRecyclerAdapter(@NonNull ArrayList<String> photoLinks, PostRecyclerFragment.OnContentPartsClickListener listener) {
        this.photoLinks = photoLinks;
        this.listener = listener;
    }

    @Override
    public DraweeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new DraweeViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_drawee, parent, false));
    }

    @Override
    public void onBindViewHolder(final DraweeViewHolder holder, final int position) {
        final String url = photoLinks.get(position);
        final Uri uri = Uri.parse(url);
        if (listener != null) {
            holder.drawee.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onPhotoClicked(holder.drawee, photoLinks, holder.getAdapterPosition());
                }
            });
        }

        ControllerListener<ImageInfo> controllerListener = new BaseControllerListener<ImageInfo>() {
            @Override
            public void onFinalImageSet(String id, final ImageInfo info, Animatable anim) {
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        if (info != null) {
                            float maxWidth = holder.width;
                            int height = holder.view.getHeight();
                            float draweeTargetWidth = (float) height / info.getHeight() * info.getWidth();
                            float viewTargetWidth = draweeTargetWidth > maxWidth ? maxWidth : draweeTargetWidth;
                            holder.drawee.setAspectRatio(draweeTargetWidth / height, true, ScalingUtils.ScaleType.FIT_CENTER);
                            holder.view.getLayoutParams().width = (int) viewTargetWidth;
                        }
                    }
                });
            }

            @Override
            public void onIntermediateImageSet(String id, @Nullable ImageInfo info) {
            }

            @Override
            public void onFailure(String id, Throwable throwable) {
            }
        };

        DraweeController controller = Fresco.newDraweeControllerBuilder()
                .setControllerListener(controllerListener)
                .setUri(uri)
                .build();

        holder.drawee.setController(controller);
    }

    @Override
    public int getItemCount() {
        return photoLinks.size();
    }
}