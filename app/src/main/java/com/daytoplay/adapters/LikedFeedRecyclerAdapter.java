package com.daytoplay.adapters;

import android.os.Handler;
import android.support.annotation.NonNull;

import com.daytoplay.fragments.BaseNewsRecyclerFragment;
import com.daytoplay.items.NewsContent;

import java.util.List;

public class LikedFeedRecyclerAdapter extends BaseFeedRecyclerAdapter {

    public LikedFeedRecyclerAdapter(@NonNull BaseNewsRecyclerFragment fragment, @NonNull List<NewsContent.NewsItem> items, BaseNewsRecyclerFragment.RecyclerListener listener) {
        super(fragment, items, listener);
    }

    @Override
    protected int getHeaderLayoutId() {
        return 0;
    }

    @Override
    protected void onItemLikedChanged(final boolean like, final NewsContent.NewsItem item, final int position) {
        Runnable r = new Runnable() {
            @Override
            public void run() {
                if (!like) {
                    List<NewsContent.NewsItem> items = newsContent.getItems();
                    int i = items.indexOf(item);
                    if (i >= 0) {
                        items.remove(i);
                        notifyItemRemoved(i);
                    }
                }
            }
        };
        new Handler().post(r);
    }
}
