package com.daytoplay.adapters;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.gg.api.PostPartItem;
import com.gg.api.PostItem;
import com.daytoplay.ErrorType;
import com.daytoplay.R;
import com.daytoplay.fragments.PostRecyclerFragment;
import com.daytoplay.utils.Logger;
import com.daytoplay.views.PhotosViewHolder;
import com.daytoplay.views.TextViewHolder;
import com.daytoplay.views.VideoViewHolder;
import com.daytoplay.utils.Utils;
import com.daytoplay.views.WebViewHolder;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class PostItemRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final List<PostPartItem> values;
    private final PostRecyclerFragment.OnContentPartsClickListener listener;
    private final Context context;
    private List<VideoViewHolder> videoHolders = new ArrayList<>();

    public PostItemRecyclerAdapter(Context context, PostItem values, PostRecyclerFragment.OnContentPartsClickListener listener) {
        this.values = values.getList();
        this.listener = listener;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        switch (viewType) {
            case PostItem.TEXT:
                return new TextViewHolder(LayoutInflater.from(context)
                        .inflate(com.daytoplay.R.layout.item_post_text, parent, false));
            case PostItem.IMAGES:
                return new PhotosViewHolder(LayoutInflater.from(context)
                        .inflate(com.daytoplay.R.layout.item_post_photos, parent, false));
            case PostItem.VIDEO:
                VideoViewHolder videoViewHolder = new VideoViewHolder(LayoutInflater.from(context)
                        .inflate(com.daytoplay.R.layout.item_post_video, parent, false));
                videoHolders.add(videoViewHolder);
                return videoViewHolder;
            case PostItem.WEB:
                return new WebViewHolder(LayoutInflater.from(context)
                        .inflate(R.layout.item_post_web, parent, false));
            default:
                Logger.e(context, ErrorType.WRONG_TYPE.toString(context) + " " + viewType);
                View itemView = new View(context);
                return new RecyclerView.ViewHolder(itemView) {
                };
        }
    }

    @Override
    public int getItemViewType(int position) {
        return values.get(position).getType();
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        int viewType = holder.getItemViewType();
        switch (viewType) {
            case PostItem.TEXT:
                bindTextHolder((TextViewHolder) holder, position);
                break;
            case PostItem.IMAGES:
                bindPhotosHolder((PhotosViewHolder) holder, position);
                break;
            case PostItem.VIDEO:
                final VideoViewHolder viewHolder = (VideoViewHolder) holder;
                Utils.setBaseRatio(viewHolder.view);
                bindVideoHolder(viewHolder, position);
                break;
            case PostItem.WEB:
                bindWebHolder((WebViewHolder) holder, position);
                break;
        }
    }

    private void bindVideoHolder(final VideoViewHolder holder, final int position) {
        holder.item = values.get(position);
        String url = holder.item.getValue();
        final String id = Utils.getYoutubeId(url);
        holder.thumbnail.setTag(id);
        holder.clickView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                releasePlayers(true);
                holder.startVideoPlayer(listener);
            }
        });
        new SetYouTubeThumbnailTask(holder.title, holder.thumbnail).execute(id);
    }

    private void bindPhotosHolder(final PhotosViewHolder holder, int position) {
        Type type = new TypeToken<List<String>>() {
        }.getType();
        ArrayList<String> photoLinks = new Gson().fromJson(values.get(position).getValue(), type);
        holder.setPhotoLinks(context, photoLinks, listener);
    }

    private void bindWebHolder(WebViewHolder holder, int position) {
        holder.item = values.get(position);
        holder.setWeb(holder.item.getValue());
    }

    private void bindTextHolder(TextViewHolder holder, int position) {
        holder.item = values.get(position);
        String text = holder.item.getValue();
        if (position > 0 && !text.trim().equals("\n") && values.get(position - 1).getType() != PostItem.TEXT) {
            View v = holder.view;
            v.setPadding(v.getLeft(), 70, v.getRight(), v.getBottom());
        }
        holder.setText(text);
    }

    @Override
    public int getItemCount() {
        return values.size();
    }

    public void releasePlayers(boolean updateThumbnails) {
        for (VideoViewHolder holder : videoHolders) {
            holder.releasePlayer(updateThumbnails);
        }
    }

    public void restorePlayer(String id, int position) {
        for (VideoViewHolder holder : videoHolders) {
            if (holder != null) {
                String videoId = holder.getVideoId();
                if (videoId != null && videoId.equals(id)) {
                    holder.startVideoPlayer(listener, position);
                }
            }
        }
    }


    class SetYouTubeThumbnailTask extends AsyncTask<String, Void, Void> {

        private String name;
        private TextView textView;
        private SimpleDraweeView thumbnail;
        private Uri uriThumbnail;

        SetYouTubeThumbnailTask(@NonNull TextView textView, @NonNull SimpleDraweeView thumbnail) {
            this.textView = textView;
            this.thumbnail = thumbnail;
        }

        @Override
        protected Void doInBackground(String... params) {
            String id = params[0];
            try {
                if (id != null) {
                    String baseYouTubeLink = "https://www.youtube.com/watch?v=";
                    URL urlName = new URL("http://www.youtube.com/oembed?url=" + baseYouTubeLink +
                            id + "&format=json");
                    name = new JSONObject(IOUtils.toString(urlName)).getString("title");
                    uriThumbnail = Uri.parse(String.format("http://img.youtube.com/vi/%s/0.jpg",
                            Uri.parse(baseYouTubeLink + id).getQueryParameter("v")));
                }
            } catch (JSONException | IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (name != null) {
                textView.setText(name);
            }
            if (uriThumbnail != null) {
                thumbnail.setImageURI(uriThumbnail);
            }
        }
    }
}
