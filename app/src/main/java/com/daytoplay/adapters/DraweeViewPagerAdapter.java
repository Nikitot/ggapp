package com.daytoplay.adapters;

import android.content.Context;
import android.net.Uri;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.daytoplay.fragments.PostRecyclerFragment;
import com.daytoplay.views.TranslateDraweeView9x16;

import java.util.ArrayList;

public class DraweeViewPagerAdapter extends PagerAdapter {
    private final PostRecyclerFragment.OnContentPartsClickListener listener;
    private ArrayList<String> photoLinks;
    private LayoutInflater layoutInflater;
    private Context context;

    public DraweeViewPagerAdapter(ArrayList<String> photoLinks, Context context, PostRecyclerFragment.OnContentPartsClickListener listener) {
        this.photoLinks = photoLinks;
        this.context = context;
        this.listener = listener;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return photoLinks.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        final View itemView = layoutInflater.inflate(com.daytoplay.R.layout.item_drawee, container, false);
        final TranslateDraweeView9x16 draweeView = (TranslateDraweeView9x16) itemView.findViewById(com.daytoplay.R.id.image);
        final String url = photoLinks.get(position);
        final Uri uri = Uri.parse(url);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onPhotoClicked(draweeView, photoLinks, position);
            }
        });
        draweeView.setImageURI(uri);
        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((FrameLayout) object);
    }

    @Override
    public float getPageWidth(int position) {
        return getCount() > 1 ? 0.85f : 1f;
    }


}
