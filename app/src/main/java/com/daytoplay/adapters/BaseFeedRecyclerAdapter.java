package com.daytoplay.adapters;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gg.api.Channel;
import com.daytoplay.Config;
import com.daytoplay.Connector;
import com.daytoplay.R;
import com.daytoplay.activities.BaseNewsActivity;
import com.daytoplay.fragments.BaseNewsRecyclerFragment;
import com.daytoplay.items.NewsContent;
import com.daytoplay.utils.Logger;
import com.daytoplay.utils.Utils;
import com.daytoplay.views.AdItemHolder;
import com.daytoplay.views.FeedItemHolder;

import java.util.List;

import retrofit2.Response;

public class BaseFeedRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    protected BaseNewsRecyclerFragment.RecyclerListener listener;
    protected BaseNewsRecyclerFragment fragment;

    public static final int HEADER = 0;
    public static final int REGULAR = 1;
    public static final int SMALL = 2;
    public static final int AD = 3;
    protected int adCount;
    protected NewsContent newsContent = new NewsContent();
    private int lastBindPosition;

    public BaseFeedRecyclerAdapter(@NonNull BaseNewsRecyclerFragment fragment,
                                   @NonNull List<NewsContent.NewsItem> items,
                                   BaseNewsRecyclerFragment.RecyclerListener listener) {
        newsContent.addEnd(items);
        this.listener = listener;
        this.fragment = fragment;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case HEADER:
                return createHeaderItemViewHolder(LayoutInflater.from(parent.getContext())
                        .inflate(getHeaderLayoutId(), parent, false));
            case SMALL:
                return new FeedItemHolder(LayoutInflater.from(parent.getContext())
                        .inflate(getSmallLayoutId(), parent, false));
            case AD:
                return new AdItemHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.ad_item, parent, false));
            case REGULAR:
                return new FeedItemHolder(LayoutInflater.from(parent.getContext())
                        .inflate(getLayoutId(), parent, false));
        }
        return null;
    }

    protected int getLayoutId() {
        return R.layout.item_image_big;
    }

    public int getSmallLayoutId() {
        return R.layout.item_image_small;
    }

    protected int getHeaderLayoutId() {
        return 0;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        lastBindPosition = position;
        if (holder != null) {
            switch (holder.getItemViewType()) {
                case HEADER:
                    bindHeaderViewHolder(holder, position);
                    break;
                case AD:
                    bindAdViewHolder((AdItemHolder) holder, position);
                    break;
                case REGULAR:
                case SMALL:
                    bindFeedViewHolder((FeedItemHolder) holder, position);
                    break;
            }
        }
    }

    public int getLastBindPosition() {
        return lastBindPosition;
    }

    private void bindAdViewHolder(final AdItemHolder holder, int position) {
        holder.refreshAd(position / Config.adPeriod - 1);
    }

    protected boolean isShowChannelInfo() {
        return true;
    }

    protected void bindFeedViewHolder(final FeedItemHolder holder, int position) {
        holder.likeView.unregister();
        if (isAdEnabled())
            position -= adCount;
        if (isCustomHeader())
            position -= 1;

        holder.reset();
        if (position == newsContent.getSize() - 1) {
            fragment.updateContent(false);
        }
        final NewsContent.NewsItem newsItem = newsContent.getItem(position);
        holder.item = newsItem;
        BaseNewsActivity activity = (BaseNewsActivity) fragment.getActivity();
        holder.likeView.init(newsItem, activity);
        holder.likeView.register();
        holder.setDetails(newsItem.details);
        holder.setTitle(newsItem.title);
        holder.viewsCount.setText(Utils.formatCount(newsItem.views_count));
        String thumbnailUrl = newsItem.thumbnail_url;
        if (thumbnailUrl != null) {
            try {
                holder.thumbnail.setImageURI(Uri.parse(thumbnailUrl));
            } catch (NullPointerException ex) {
                Logger.e(holder.view.getContext(), ex.getMessage());
            }
        }

        int channelId = newsItem.news_item_type;
        final Channel channel = Config.getChannel(channelId);

        boolean isListenerNonNull = listener != null;
        if (channel != null) {
            if (isShowChannelInfo()) {
                initChannel(channel, newsItem, holder);
                if (isListenerNonNull) {
                    holder.headerLayout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            listener.onChannelClicked(channel);
                        }
                    });
                }
            } else {
                holder.channelImageView.setVisibility(View.GONE);
                holder.channelTextView.setVisibility(View.GONE);

            }
        } else {
            Connector.createService().getChannel(channelId).enqueue(new Connector.Callback<Channel>(holder.view.getContext()) {
                @Override
                protected void onSuccess(Response<Channel> response, String url) {
                    super.onSuccess(response, url);
                    Channel channel = response.body();
                    Config.get().channels.add(channel);
                    initChannel(channel, newsItem, holder);
                }
            });
        }

        holder.dateView.setText(newsItem.getCreatedDate());

        if (isListenerNonNull) {
            holder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.viewsCount.setText(Utils.formatCount(newsItem.views_count++));
                    listener.onRecyclerItemClicked(holder);
                }
            });

            holder.shareView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onShareClicked(holder.item.link);
                }
            });
        }
    }

    private void initChannel(Channel channel, NewsContent.NewsItem newsItem, FeedItemHolder holder) {
        newsItem.channel = channel;
        holder.channelImageView.setImageURI(channel.logo);
        holder.channelTextView.setText(channel.name);
    }

    protected void bindHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0 && isCustomHeader())
            return HEADER;
        else {
            double firstAdPosition = Config.adPeriod / 2.75;
            if (isAdEnabled() && (position == Math.round(firstAdPosition) || position % Config.adPeriod == 0))
                return AD;
            else
                return REGULAR;
        }
    }

    private boolean isCustomHeader() {
        return getHeaderLayoutId() != 0;
    }

    @Override
    public int getItemCount() {
        return newsContent.getSize() + adCount + (isCustomHeader() ? 1 : 0);
    }

    @NonNull
    public List<NewsContent.NewsItem> getItems() {
        return newsContent.getItems();
    }

    public NewsContent.NewsItem getItem(int position) {
        return newsContent.getItem(position);
    }

    public void addItems(List<NewsContent.NewsItem> newsItems, boolean top) {
        List<NewsContent.NewsItem> oldItems = newsContent.getItems();
        for (int i = 0; i < newsItems.size(); i++) {
            NewsContent.NewsItem newItem = newsItems.get(i);
            for (NewsContent.NewsItem oldItem : oldItems) {
                if (newItem.id == oldItem.id) {
                    newsItems.remove(i);
                    i--;
                    break;
                }
            }
        }

        int prevCount = getItemCount();
        int newCount = newsItems.size();
        if (top) {
            newsContent.addTop(newsItems);
            if (newCount < 10) {
                notifyItemRangeInserted(0, newCount);
            } else {
                notifyDataSetChanged();
                fragment.onDataReset();
            }
        } else {
            newsContent.addEnd(newsItems);
            this.notifyItemRangeInserted(prevCount, newCount);
        }
    }

    public RecyclerView.ViewHolder createHeaderItemViewHolder(View inflate) {
        return null;
    }


    public void updateItemLike(boolean like, long newsId, int count) {
        List<NewsContent.NewsItem> items = newsContent.getItems();
        for (int i = 0; i < items.size(); i++) {
            NewsContent.NewsItem item = items.get(i);
            if (item.id == newsId) {
                item.likes_count = count;
                onItemLikedChanged(like, item, i);
            }
        }
    }

    protected void onItemLikedChanged(boolean like, NewsContent.NewsItem item, int position) {
    }

    protected boolean isAdEnabled() {
        return false;
    }
}
