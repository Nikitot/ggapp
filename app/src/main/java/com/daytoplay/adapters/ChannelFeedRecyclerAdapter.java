package com.daytoplay.adapters;

import android.support.annotation.NonNull;

import com.daytoplay.fragments.BaseNewsRecyclerFragment;
import com.daytoplay.items.NewsContent;

import java.util.List;

public class ChannelFeedRecyclerAdapter extends BaseFeedRecyclerAdapter {

    public ChannelFeedRecyclerAdapter(@NonNull BaseNewsRecyclerFragment fragment, @NonNull List<NewsContent.NewsItem> items, BaseNewsRecyclerFragment.RecyclerListener listener) {
        super(fragment, items, listener);
    }

    protected boolean isShowChannelInfo() {
        return false;
    }

}
