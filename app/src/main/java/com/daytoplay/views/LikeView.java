package com.daytoplay.views;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daytoplay.Config;
import com.daytoplay.Connector;
import com.daytoplay.ErrorType;
import com.daytoplay.Observer;
import com.daytoplay.R;
import com.daytoplay.items.LikeItem;
import com.daytoplay.items.NewsContent;
import com.daytoplay.utils.Logger;
import com.daytoplay.utils.Utils;
import com.google.firebase.auth.FirebaseUser;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Response;

public class LikeView extends LinearLayout implements View.OnClickListener, Observer {

    public static final int NONE = -1;
    public static final int NOT_INIT = -2;
    public long postId = NONE;
    public long likeId = NOT_INIT;
    public int initCount;
    public int count;
    public boolean initLiked;
    public MessageHandler handler = new MessageHandler(this);

    ImageButton likeButton;
    TextView countView;

    @Nullable
    LikeListener likeListener;

    public LikeView(Context context) {
        this(context, null);
    }

    public LikeView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LikeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        View layout = inflate(context, R.layout.view_like, this);
        likeButton = (ImageButton) layout.findViewById(R.id.like_button);
        countView = (TextView) layout.findViewById(R.id.like_count);
        likeButton.setOnClickListener(this);
    }

    public void init(NewsContent.NewsItem item, LikeListener likeListener) {
        disable();
        this.likeListener = likeListener;
        this.postId = item.id;
        initRequests(postId);
    }

    private void disable() {
        postId = NONE;
        likeId = NOT_INIT;
        count = 0;
        initCount = 0;
        updateCount();
        likeButton.setAlpha(0.5f);
        likeButton.setImageResource(R.drawable.ic_favorite_white_24dp);
        likeButton.setClickable(false);
    }

    private void initRequests(final long postId) {
        if (this.postId == postId) {
            Connector.createService()
                    .getLikesCount(this.postId).enqueue(new Connector.Callback<Integer>(getContext()) {
                @Override
                protected void onSuccess(Response<Integer> response, String url) {
                    super.onSuccess(response, url);
                    handler.removeMessages(MessageHandler.INIT_REQUESTS);
                    count = initCount = response.body();
                    FirebaseUser user = Config.auth.getCurrentUser();
                    if (user != null) {
                        checkLike(postId, user.getUid(), false);
                    } else {
                        updateCount();
                        likeButton.setClickable(true);
                    }
                }

                @Override
                protected void onError(ErrorType errorType, String msg, String url) {
                    super.onError(errorType, msg, url);
                    handler.removeMessages(MessageHandler.INIT_REQUESTS);
                    handler.sendMessage(Message.obtain(handler, MessageHandler.INIT_REQUESTS, postId));
                }
            });
        }
    }

    public void checkLike(final long postId, final String userId, final boolean notify) {
        Connector.createService()
                .getLike(postId, userId).enqueue(new Connector.Callback<List<LikeItem>>(getContext()) {
            @Override
            protected void onSuccess(Response<List<LikeItem>> response, String url) {
                super.onSuccess(response, url);
                handler.removeMessages(MessageHandler.CHECK_LIKE);
                List<LikeItem> body = response.body();
                boolean isInitial = likeId == NOT_INIT;
                if (body.size() > 0) {
                    likeId = body.get(0).id;
                    setCheckedLike(true);
                } else {
                    likeId = NONE;
                    setCheckedLike(false);
                }

                boolean liked = isLiked();
                if (isInitial) initLiked = liked;
                likeButton.setClickable(true);
                if (notify) syncLikeView();
            }

            @Override
            protected void onError(ErrorType errorType, String msg, String url) {
                super.onError(errorType, msg, url);
                handler.removeMessages(MessageHandler.CHECK_LIKE);
                handler.sendMessage(Message.obtain(handler, MessageHandler.CHECK_LIKE, new CheckLikeInfo(postId, userId, notify)));
            }
        });
    }

    private void syncLikeView() {
        Config.notifyObservers(LikeView.this);
    }

    public void toggle(final String userId) {
        final Context context = getContext();
        if (!Utils.isConneted(context)) {
            Utils.toast(context, getResources().getString(ErrorType.NO_CONNECTION.textId));
            return;
        }
        likeButton.setClickable(false);
        if (!isLiked()) {
            setSelectedLikeView(true);
            syncLikeView();
            HashMap<String, Object> body = new HashMap<>();
            body.put("news_item_id", postId);
            body.put("user_id", userId);
            Connector.createService()
                    .like(body).enqueue(new Connector.Callback<ResponseBody>(context) {
                @Override
                protected void onSuccess(Response<ResponseBody> response, String url) {
                    Logger.i(context, userId + " user liked the post " + postId);
                    checkLike(postId, userId, true);
                }

                @Override
                protected void onError(ErrorType errorType, String msg, String url) {
                    super.onError(errorType, msg, url);
                    setSelectedLikeView(false);
                    likeButton.setClickable(true);
                    syncLikeView();
                }
            });
        } else {
            setSelectedLikeView(false);
            syncLikeView();
            Connector.createService()
                    .unlike(likeId).enqueue(new Connector.Callback<ResponseBody>(context) {
                @Override
                protected void onSuccess(Response<ResponseBody> response, String url) {
                    Logger.i(context, userId + " user unlike the post " + postId);
                    checkLike(postId, userId, true);
                }

                @Override
                protected void onError(ErrorType errorType, String msg, String url) {
                    super.onError(errorType, msg, url);
                    setSelectedLikeView(true);
                    likeButton.setClickable(true);
                    syncLikeView();
                }
            });
        }
    }

    public void setSelectedLikeView(boolean selected) {
        if (selected != likeButton.isSelected()) {
            if (initLiked) count = selected ? initCount : initCount - 1;
            else count = selected ? initCount + 1 : initCount;
            setCheckedLike(selected);
            onLikedChanged(selected);
        }
    }

    protected void onLikedChanged(boolean like) {
        if (likeListener != null) {
            likeListener.onLikedChanged(like, postId, count);
        }
    }

    public void setCheckedLike(boolean like) {
        likeButton.setAlpha(1f);
        likeButton.setSelected(like);
        if (like) {
            likeButton.setImageBitmap(Utils.getGlowLikeBitmap(getContext()));
        } else {
            likeButton.setImageResource(R.drawable.ic_favorite_border_white_24dp);
        }
        updateCount();
    }

    protected void updateCount() {
        countView.setText(Utils.formatCount(count));
        if (count > 0) {
            countView.setVisibility(VISIBLE);
        } else {
            countView.setVisibility(GONE);
        }
    }

    public boolean isLiked() {
        return likeId != NONE;
    }

    @Override
    public void onClick(View view) {
        FirebaseUser user = Config.auth.getCurrentUser();
        if (user != null) {
            if (postId != NONE) {
                toggle(user.getUid());
            }
        } else {
            Utils.toast(getContext(), getResources().getString(R.string.not_logged_in));
        }
    }

    public void reset() {
        disable();

    }

    @Override
    public void objectModified(Object obj) {
        if (obj instanceof LikeView) {
            LikeView lv = (LikeView) obj;
            if (lv != this && lv.postId == postId) {
                count = lv.count;
                initCount = lv.initCount;
                likeId = lv.likeId;
                initLiked = lv.initLiked;
                likeButton.setClickable(lv.likeButton.isClickable());
                boolean like = isLiked();
                setCheckedLike(like);
                onLikedChanged(like);
            }
        }
    }

    public void register() {
        Config.registerObserver(this);
    }

    public void unregister() {
        handler.removeMessages(MessageHandler.CHECK_LIKE);
        handler.removeMessages(MessageHandler.INIT_REQUESTS);
        Config.unregisterObserver(this);
    }

    public interface LikeListener {
        void onLikedChanged(boolean like, long newsId, int count);
    }

    private class CheckLikeInfo {
        public long postId;
        public String userId;
        public boolean notify;

        public CheckLikeInfo(long postId, String userId, boolean notify) {
            this.postId = postId;
            this.userId = userId;
            this.notify = notify;
        }
    }

    private static class MessageHandler extends Handler {

        public static final int CHECK_LIKE = 1;
        public static final int INIT_REQUESTS = 2;
        private final WeakReference<LikeView> likeView;

        MessageHandler(LikeView view) {
            likeView = new WeakReference<>(view);
        }

        @Override
        public void handleMessage(Message msg) {
            LikeView likeView = this.likeView.get();
            if (likeView != null) {
                switch (msg.what) {
                    case (CHECK_LIKE):
                        CheckLikeInfo info = (CheckLikeInfo) msg.obj;
                        likeView.checkLike(info.postId, info.userId, info.notify);
                        break;

                    case (INIT_REQUESTS):
                        long postId = (Long) msg.obj;
                        likeView.initRequests(postId);
                        break;
                }
            }
        }
    }
}
