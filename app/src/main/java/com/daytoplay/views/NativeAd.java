package com.daytoplay.views;

import android.support.annotation.NonNull;
import android.view.View;

import com.daytoplay.BuildConfig;
import com.daytoplay.Config;
import com.daytoplay.R;
import com.daytoplay.activities.BaseNewsActivity;
import com.daytoplay.utils.Utils;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.NativeExpressAdView;

public class NativeAd {
    //360x132, R.string.ad_unit_id_native_s

    private static final float MIN_WIDTH_DP = 280;
    private static final float MIN_HEIGHT_DP = 250;
    private static final int AD_UNIT_ID = R.string.ad_unit_id_native;
    private static final float RATIO = MIN_HEIGHT_DP / MIN_WIDTH_DP;

    private static NativeAd ourInstance = new NativeAd();
    private static String ROOT_DEVICE_ID;
    private NativeExpressAdView adView;

    public static NativeAd getInstance(@NonNull BaseNewsActivity activity) {
        ROOT_DEVICE_ID = activity.getString(R.string.root_device_id);
        return ourInstance.init(activity);
    }

    private NativeAd init(@NonNull BaseNewsActivity activity) {
        adView = new NativeExpressAdView(activity);
        int width = Math.max(Config.defaultSize.x, Utils.dpToPixel(activity, MIN_WIDTH_DP));
        int height = Math.max((int) (width * RATIO), Utils.dpToPixel(activity, MIN_HEIGHT_DP));
        adView.setAdSize(new AdSize(Utils.pixelsToDp(activity, width), Utils.pixelsToDp(activity, height)));
        adView.setAdUnitId(activity.getString(AD_UNIT_ID));
        hide();
        refresh();
        return this;
    }

    public NativeExpressAdView getView() {
        return adView;
    }

    void refresh() {
        hide();
        AdRequest.Builder builder = new AdRequest.Builder();
        if (BuildConfig.DEBUG) {
            builder.addTestDevice(Config.deviceId);
        }
        builder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);
        builder.addTestDevice(ROOT_DEVICE_ID);
        adView.loadAd(builder.build());
        adView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                show();
            }
        });
    }

    public void hide() {
        adView.setVisibility(View.INVISIBLE);

    }

    public void show() {
        adView.setVisibility(View.VISIBLE);
    }
}
