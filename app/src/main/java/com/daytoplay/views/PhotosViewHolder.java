package com.daytoplay.views;

import android.content.Context;
import android.graphics.drawable.Animatable;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.imagepipeline.image.ImageInfo;
import com.daytoplay.Config;
import com.daytoplay.R;
import com.daytoplay.adapters.ResizableDraweeRecyclerAdapter;
import com.daytoplay.fragments.PostRecyclerFragment;
import com.daytoplay.utils.Utils;

import java.util.ArrayList;

public class PhotosViewHolder extends RecyclerView.ViewHolder {
    public RecyclerView recycler;
    public TranslateDraweeView9x16 singleImage;
    public View view;

    public PhotosViewHolder(View view) {
        super(view);
        this.view = view;
        recycler = (RecyclerView) view.findViewById(R.id.recycler);
        recycler.setNestedScrollingEnabled(false);
        singleImage = (TranslateDraweeView9x16) view.findViewById(R.id.single_image);
    }

    public void setPhotoLinks(Context context, final ArrayList<String> photoLinks, @NonNull final PostRecyclerFragment.OnContentPartsClickListener listener) {
        int size = photoLinks.size();
        if (size > 0) {
            if (size == 1) {

                ControllerListener<ImageInfo> controllerListener = new BaseControllerListener<ImageInfo>() {
                    @Override
                    public void onFinalImageSet(String id, final ImageInfo info, Animatable anim) {
                        new Handler().post(new Runnable() {
                            @Override
                            public void run() {
                                if (info != null) {
                                    int width = singleImage.getWidth();
                                    float targetHeight = (float) width / info.getWidth() * info.getHeight();
                                    singleImage.setAspectRatio(width / targetHeight);
                                }
                            }
                        });
                    }

                    @Override
                    public void onIntermediateImageSet(String id, @Nullable ImageInfo info) {
                    }

                    @Override
                    public void onFailure(String id, Throwable throwable) {
                    }
                };

                final String url = photoLinks.get(0);
                DraweeController controller = Fresco.newDraweeControllerBuilder()
                        .setControllerListener(controllerListener)
                        .setUri(Uri.parse(url))
                        .build();
                singleImage.setController(controller);
                singleImage.setVisibility(View.VISIBLE);
                singleImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        listener.onPhotoClicked(singleImage, photoLinks, 0);
                    }
                });
            } else {
                recycler.setVisibility(View.VISIBLE);
                recycler.getLayoutParams().height = Math.round(Config.defaultSize.x * Utils.ratio9x16);
                recycler.setAdapter(new ResizableDraweeRecyclerAdapter(photoLinks, listener));
                recycler.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                Utils.setBaseRatio(recycler);
            }
        } else {
            view.setVisibility(View.GONE);
        }
    }
}
