package com.daytoplay.views;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.daytoplay.Config;
import com.daytoplay.R;

public class DraweeViewHolder extends RecyclerView.ViewHolder {
    public final int width;
    public TranslateDraweeView9x16 drawee;
    public View view;

    public DraweeViewHolder(final View view) {
        super(view);
        this.view = view;
        final int displayWidth = Config.defaultSize.x;
        width = displayWidth - Math.round(displayWidth / 15f);
        drawee = (TranslateDraweeView9x16) view.findViewById(R.id.image);
        drawee.getLayoutParams().width = width;
    }
}
