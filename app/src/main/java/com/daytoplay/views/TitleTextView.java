package com.daytoplay.views;

import android.content.Context;
import android.util.AttributeSet;

public class TitleTextView extends android.widget.TextView {
    public TitleTextView(Context context) {
        this(context, null);
    }

    public TitleTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TitleTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        if (text != null) {
            super.setText(text.toString().replace("\\\"", "\""), type);
        }
    }

}
