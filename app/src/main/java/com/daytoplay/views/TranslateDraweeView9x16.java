package com.daytoplay.views;

import android.content.Context;
import android.util.AttributeSet;

import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.daytoplay.utils.Utils;

public class TranslateDraweeView9x16 extends TranslateDraweeView {

    public TranslateDraweeView9x16(Context context) {
        super(context);
        init();
    }

    public TranslateDraweeView9x16(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TranslateDraweeView9x16(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    public TranslateDraweeView9x16(Context context, GenericDraweeHierarchy hierarchy) {
        super(context, hierarchy);
        init();
    }

    public TranslateDraweeView9x16(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public void init() {
        setAspectRatio(Utils.ratio16x9);
    }
}