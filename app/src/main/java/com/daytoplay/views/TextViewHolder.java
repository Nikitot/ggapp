package com.daytoplay.views;

import android.support.v7.widget.RecyclerView;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.TextView;

import com.daytoplay.R;
import com.gg.api.PostPartItem;
import com.daytoplay.SafeURLSpan;

public class TextViewHolder extends RecyclerView.ViewHolder {
    public final View view;
    public final TextView textView;
    public PostPartItem item;

    public TextViewHolder(View view) {
        super(view);
        this.view = view;
        textView = (TextView) view.findViewById(R.id.text);
    }

    public void setText(String htmlText){
        textView.setLinksClickable(true);
        textView.setText(SafeURLSpan.parseSafeHtml(htmlText));
        textView.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @Override
    public String toString() {
        return super.toString() + " '" + textView.getText() + "'";
    }
}
