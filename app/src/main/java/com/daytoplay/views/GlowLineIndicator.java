package com.daytoplay.views;

import android.content.Context;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.view.View;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;

import com.daytoplay.R;

import net.lucode.hackware.magicindicator.buildins.UIUtil;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.model.PositionData;

import java.util.List;

public class GlowLineIndicator extends View implements IPagerIndicator {
    private Interpolator startInterpolator = new LinearInterpolator();
    private Interpolator endInterpolator = new LinearInterpolator();
    private float lineHeight;
    private float roundRadius;
    private float glowRadius;
    private Paint paint;
    private List<PositionData> positionDataList;
    private RectF lineRect = new RectF();

    public GlowLineIndicator(Context context) {
        super(context);
        this.setLayerType(android.view.View.LAYER_TYPE_SOFTWARE, null);
        setGlowRadiusDp(3);
        setLineHeightDp(3);
        paint = new Paint(
                Paint.ANTI_ALIAS_FLAG | Paint.DITHER_FLAG);
        paint.setColor(getResources().getColor(R.color.color_indicator0));
        paint.setStyle(Paint.Style.FILL);
        paint.setMaskFilter(new BlurMaskFilter(glowRadius, BlurMaskFilter.Blur.SOLID));
    }

    protected void onDraw(Canvas canvas) {
        canvas.drawRoundRect(this.lineRect, this.roundRadius, this.roundRadius, this.paint);
    }

    public void setGlowRadiusDp(int glowRadius) {
        this.glowRadius = UIUtil.dip2px(getContext(), glowRadius);
        this.lineHeight += this.glowRadius;
    }

    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        if (this.positionDataList != null && !this.positionDataList.isEmpty()) {
            int currentPosition = Math.min(this.positionDataList.size() - 1, position);
            int nextPosition = Math.min(this.positionDataList.size() - 1, position + 1);
            PositionData current = this.positionDataList.get(currentPosition);
            PositionData next = this.positionDataList.get(nextPosition);
            float leftX;
            float nextLeftX;
            float rightX;
            float nextRightX;
            leftX = (float) current.mLeft;
            nextLeftX = (float) next.mLeft;
            rightX = (float) current.mRight;
            nextRightX = (float) next.mRight;
            float glowPadding = this.glowRadius / 2;
            this.lineRect.left = leftX + (nextLeftX - leftX) * this.startInterpolator.getInterpolation(positionOffset) + glowPadding;
            this.lineRect.right = rightX + (nextRightX - rightX) * this.endInterpolator.getInterpolation(positionOffset) - glowPadding;
            this.lineRect.top = (float) this.getHeight() - this.lineHeight + glowPadding;
            this.lineRect.bottom = (float) this.getHeight() - glowPadding;
            this.invalidate();
        }
    }

    public void onPageSelected(int position) {
    }

    public void onPageScrollStateChanged(int state) {
    }

    public void onPositionDataProvide(List<PositionData> dataList) {
        this.positionDataList = dataList;
    }

    public void setLineHeightDp(float lineHeight) {
        this.lineHeight = UIUtil.dip2px(getContext(), lineHeight);
    }

    public void setRoundRadiusDp(int roundRadius) {
        this.roundRadius = UIUtil.dip2px(getContext(), roundRadius);
    }

    public void setStartInterpolator(Interpolator startInterpolator) {
        this.startInterpolator = startInterpolator;
        if (this.startInterpolator == null) {
            this.startInterpolator = new LinearInterpolator();
        }

    }

    public void setEndInterpolator(Interpolator endInterpolator) {
        this.endInterpolator = endInterpolator;
        if (this.endInterpolator == null) {
            this.endInterpolator = new LinearInterpolator();
        }

    }

}
