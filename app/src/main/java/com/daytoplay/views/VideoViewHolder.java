package com.daytoplay.views;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.gg.api.PostPartItem;
import com.daytoplay.Config;
import com.daytoplay.ErrorType;
import com.daytoplay.R;
import com.daytoplay.fragments.PostRecyclerFragment;
import com.daytoplay.utils.Utils;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

public class VideoViewHolder extends RecyclerView.ViewHolder {
    public final View view;
    public final SimpleDraweeView thumbnail;
    public final TextView title;
    public final View clickView;
    public final View thumbnailLayout;
    private final RelativeLayout playerLayout;
    private String id;
    public PostPartItem item;
    private YouTubePlayer youTubePlayer;

    public VideoViewHolder(final View view) {
        super(view);
        this.view = view;
        thumbnail = (SimpleDraweeView) view.findViewById(R.id.thumbnail);
        title = (TextView) view.findViewById(R.id.title);
        clickView = view.findViewById(R.id.start_payer_button);
        thumbnailLayout = view.findViewById(R.id.thumbnail_layout);
        playerLayout = (RelativeLayout) view.findViewById(R.id.player_layout);
    }

    public void startVideoPlayer(final PostRecyclerFragment.OnContentPartsClickListener listener) {
        startVideoPlayer(listener, 0);
    }

    public void startVideoPlayer(final PostRecyclerFragment.OnContentPartsClickListener listener, final int position) {
        id = Utils.getYoutubeId(item.getValue());
        YouTubePlayerView youTubeView = new YouTubePlayerView(view.getContext());
        playerLayout.addView(youTubeView, FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
        youTubeView.initialize(Config.YOUTUBE_API_KEY, new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, final YouTubePlayer youTubePlayer, boolean wasRestored) {
                if (!wasRestored) {
                    youTubePlayer.setFullscreenControlFlags(YouTubePlayer.FULLSCREEN_FLAG_ALWAYS_FULLSCREEN_IN_LANDSCAPE |
                            YouTubePlayer.FULLSCREEN_FLAG_CONTROL_ORIENTATION |
                            YouTubePlayer.FULLSCREEN_FLAG_CONTROL_SYSTEM_UI);
                    thumbnailLayout.setVisibility(View.GONE);
                    playerLayout.setVisibility(View.VISIBLE);
                    youTubePlayer.loadVideo(id, position);
                    VideoViewHolder.this.youTubePlayer = youTubePlayer;
                }
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
                listener.onError(ErrorType.YOUTUBE);
            }
        });
    }

    public void releasePlayer(boolean showThumbnail) {
        if (thumbnailLayout.getVisibility() != View.VISIBLE && showThumbnail) {
            thumbnailLayout.setVisibility(View.VISIBLE);
        }
        if (youTubePlayer != null) {
            youTubePlayer.release();
        }
        playerLayout.setVisibility(View.GONE);
    }

    public String getVideoId() {
        return id;
    }
}
