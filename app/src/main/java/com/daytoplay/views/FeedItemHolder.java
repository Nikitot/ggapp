package com.daytoplay.views;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.daytoplay.R;
import com.daytoplay.SafeURLSpan;
import com.daytoplay.items.NewsContent;
import com.daytoplay.utils.Utils;
import com.google.firebase.analytics.FirebaseAnalytics;

public class FeedItemHolder extends RecyclerView.ViewHolder {
    public final View view;
    public final TranslateDraweeView9x16 thumbnail;
    public final TextView title;
    public final TextView details;
    public final TranslateDraweeView9x16 channelImageView;
    public final TextView channelTextView;
    public final TextView dateView;
    public final ImageButton shareView;
    public final LikeView likeView;
    public final TextView viewsCount;
    public final View headerLayout;
    public NewsContent.NewsItem item;

    public FeedItemHolder(View view) {
        super(view);
        this.view = view;
        title = (TitleTextView) view.findViewById(R.id.title);
        details = (TitleTextView) view.findViewById(R.id.details);
        thumbnail = (TranslateDraweeView9x16) view.findViewById(R.id.thumbnail);
        headerLayout = view.findViewById(R.id.feed_header_layout);
        channelImageView = (TranslateDraweeView9x16) view.findViewById(R.id.channel_image);
        channelTextView = (TextView) view.findViewById(R.id.root_text);
        Utils.roundCrop(channelImageView);
        dateView = (TextView) view.findViewById(R.id.date);
        shareView = (ImageButton) view.findViewById(R.id.share);
        viewsCount = (TextView) view.findViewById(R.id.views_count);
        likeView = (LikeView) view.findViewById(R.id.like);
    }


    @Override
    public String toString() {
        return super.toString() + " '" + title.getText() + "'";
    }

    public void setTitle(String titleText) {
        title.setText(titleText);
    }

    public void setDetails(String detailsText) {
        if (detailsText == null || detailsText.equals("null") || detailsText.length() == 0) {
            details.setVisibility(View.GONE);
        } else {
            details.setText(SafeURLSpan.parseSafeHtml(detailsText));
        }
    }

    public Bundle getParams() {
        Bundle b = new Bundle();
        b.putString(FirebaseAnalytics.Param.ITEM_ID, String.valueOf(item.id));
        b.putString(FirebaseAnalytics.Param.ITEM_NAME, this.getClass().getName());
        b.putString(FirebaseAnalytics.Param.LOCATION, view.getParent().getClass().getName());
        return b;
    }

    /**
     * need for recycler
     */
    public void reset() {
        channelImageView.setImageResource(0);
        likeView.reset();
    }
}
