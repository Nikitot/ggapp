package com.daytoplay.views;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.gg.api.PostPartItem;
import com.daytoplay.R;
import com.daytoplay.utils.Utils;

public class WebViewHolder extends RecyclerView.ViewHolder {
    public View view;
    private WebView webView;
    public PostPartItem item;


    @SuppressLint("SetJavaScriptEnabled")
    public WebViewHolder(final View view) {
        super(view);
        this.view = view;
        webView = (WebView) view.findViewById(R.id.web);
        webView.setWebViewClient(new CustomWebViewClient());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webView.setBackgroundColor(Color.TRANSPARENT);
        webView.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
    }

    public void setWeb(String content) {
        webView.loadData(Utils.fromJsonHtml(content), "text/html", null);
    }

    private class CustomWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
}
