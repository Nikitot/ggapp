package com.daytoplay.views;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;

import com.daytoplay.R;
import com.daytoplay.activities.FeedActivity;
import com.daytoplay.items.PageInfo;
import com.daytoplay.utils.AnimationHelper;
import com.daytoplay.utils.Utils;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.ViewPagerHelper;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.CommonPagerTitleView;

import java.util.ArrayList;
import java.util.List;

public class SectionIndicatorView extends MagicIndicator {


    private List<CommonPagerTitleView> titleViews;

    public SectionIndicatorView(Context context) {
        this(context, null);
    }

    public SectionIndicatorView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void bind(@NonNull final List<PageInfo> pageInfos, final ViewPager viewPager) {
        titleViews = new ArrayList<>(pageInfos.size());
        CommonNavigator commonNavigator = new CommonNavigator(getContext());
        commonNavigator.setAdapter(new CommonNavigatorAdapter() {

            @Override
            public int getCount() {
                return pageInfos.size();
            }

            @Override
            public IPagerTitleView getTitleView(final Context context, final int index) {
                final CommonPagerTitleView commonPagerTitleView = new CommonPagerTitleView(context);
                titleViews.add(index, commonPagerTitleView);
                commonPagerTitleView.setContentView(R.layout.indicator_title);
                commonPagerTitleView.setVisibility(GONE);
                final PageInfo pageInfo = pageInfos.get(index);

                final ImageView icon = (ImageView) commonPagerTitleView.findViewById(R.id.title_img);
                final View notify = commonPagerTitleView.findViewById(R.id.notify);
//                switch (pageInfo.type) {
//                    case NEW:
//                    case TOP:
//                        AnimationHelper.fadeShow(notify, 300);
//                        notify.setVisibility(VISIBLE);
//                        break;
//                }
                icon.setImageResource(pageInfo.icon);
                commonPagerTitleView.post(new Runnable() {
                    @Override
                    public void run() {
                        commonPagerTitleView.getLayoutParams().width = SectionIndicatorView.this.getWidth() / FeedActivity.SectionType.size();
                        commonPagerTitleView.requestLayout();
                        commonPagerTitleView.setVisibility(VISIBLE);
                    }
                });
                commonPagerTitleView.setOnPagerTitleChangeListener(new CommonPagerTitleView.OnPagerTitleChangeListener() {
                    FeedActivity.SectionType[] values = FeedActivity.SectionType.values();

                    @Override
                    public void onSelected(int index, int totalCount) {
                        AnimationHelper.fadeHide(notify, 300);
                        icon.animate().scaleX(1.1f).scaleY(1.1f).alpha(1f).start();

                        if (context instanceof Listener) {
                            ((Listener) context).onSectionSelected(values[index]);
                        }
                    }

                    @Override
                    public void onDeselected(int index, int totalCount) {
                        icon.animate().scaleX(0.8f).scaleY(0.8f).alpha(0.7f).start();
                    }

                    @Override
                    public void onLeave(int index, int totalCount, float leavePercent, boolean leftToRight) {
                    }

                    @Override
                    public void onEnter(int index, int totalCount, float enterPercent, boolean leftToRight) {
                    }
                });

                commonPagerTitleView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        viewPager.setCurrentItem(index);
                    }
                });

                return commonPagerTitleView;
            }

            @Override
            public IPagerIndicator getIndicator(Context context) {
                LinePagerIndicator indicator = new LinePagerIndicator(context);
                indicator.setStartInterpolator(new AccelerateInterpolator());
                indicator.setEndInterpolator(new DecelerateInterpolator(1.6f));
                indicator.setLineHeight(Utils.dpToPixel(context, 1.5f));
                indicator.setColors(getResources().getColor(R.color.color_indicator1), getResources().getColor(R.color.color_indicator0));
                return indicator;
            }
        });
        setNavigator(commonNavigator);
        ViewPagerHelper.bind(this, viewPager);
    }

    public void hopCurrent(int i) {
        final CommonPagerTitleView view = titleViews.get(i);
        final float initialScale = getScaleX();
        view.animate()
                .scaleX(1.4f)
                .scaleY(1.4f)
                .setDuration(100)
                .setInterpolator(new DecelerateInterpolator())
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        view.animate()
                                .scaleX(initialScale)
                                .scaleY(initialScale)
                                .setDuration(100)
                                .setInterpolator(new AccelerateInterpolator())
                                .setListener(null);
                    }
                });
    }

    public interface Listener {
        void onSectionSelected(FeedActivity.SectionType type);
    }
}
