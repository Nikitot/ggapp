package com.daytoplay.views;

import android.content.Context;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.AttributeSet;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.drawable.ScalingUtils;
import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.view.SimpleDraweeView;

public class TranslateDraweeView extends SimpleDraweeView {

    public TranslateDraweeView(Context context) {
        super(context);
    }

    public TranslateDraweeView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TranslateDraweeView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public TranslateDraweeView(Context context, GenericDraweeHierarchy hierarchy) {
        super(context, hierarchy);
    }

    public TranslateDraweeView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void setImageURI(Uri uri) {
        if (uri.toString().contains(".gif")) {
            setController(Fresco.newDraweeControllerBuilder()
                    .setUri(uri)
                    .setAutoPlayAnimations(true)
                    .build());
        } else {
            super.setImageURI(uri);
        }
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        Drawable drawable = getTopLevelDrawable();
        if (drawable != null) {
            drawable.setBounds(0, 0, w, h);
        }
    }

    public void setAspectRatio(final float aspectRatio, boolean wrapWidth, ScalingUtils.ScaleType centerCrop) {
        if (wrapWidth) {
            getLayoutParams().width = (int) (getHeight() * aspectRatio);
            getHierarchy().setActualImageScaleType(centerCrop);
        }
        super.setAspectRatio(aspectRatio);
    }

    // looks likeButton overwrite this method can fix this issue
    // but still don't figure out why
    public void animateTransform(Matrix matrix) {
        invalidate();
    }
}