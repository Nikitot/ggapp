package com.daytoplay.views;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

import com.daytoplay.R;

public class YouTubeCustomToolbar extends RelativeLayout {
    public static final int DURATION = 300;
    private final View background;
    private final TitleTextView title;
    private boolean isVisibleTitle;

    public YouTubeCustomToolbar(Context context) {
        this(context, null);
    }

    public YouTubeCustomToolbar(Context context, AttributeSet attrs) {
        super(context, attrs);
        View layout = inflate(context, com.daytoplay.R.layout.toolbar_custom, this);
        background = layout.findViewById(R.id.background);
        title = (TitleTextView) layout.findViewById(R.id.title);
    }

    public void setTitle(String text) {
        this.title.setText(text);
    }

    public void showBackground() {
        if (!isVisibleTitle) {
            isVisibleTitle = true;
            background.setAlpha(1f);
            title.setAlpha(1f);
            background.setVisibility(VISIBLE);
            title.setVisibility(VISIBLE);
        }
    }

    public void hideTitle() {
        if (isVisibleTitle) {
            isVisibleTitle = false;
            hideViewAnimation(title);
            hideViewAnimation(background);
        }
    }

    public void hideViewAnimation(final View view) {
        view.setVisibility(VISIBLE);
        view.animate()
                .alpha(0f)
                .setDuration(DURATION)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.setVisibility(GONE);
                        super.onAnimationEnd(animation);
                    }
                }).start();
    }
}
