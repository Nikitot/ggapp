package com.daytoplay.views;

import android.content.Context;
import android.util.AttributeSet;

import com.facebook.drawee.generic.GenericDraweeHierarchy;

import java.util.Random;

public class MovingView extends TranslateDraweeView {
    final Random random = new Random();

    public MovingView(Context context) {
        super(context);
    }

    public MovingView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public MovingView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MovingView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public MovingView(Context context, GenericDraweeHierarchy hierarchy) {
        super(context, hierarchy);
    }

    @Override
    public void setImageURI(String uriString) {
        super.setImageURI(uriString);
        float value = 1 + random.nextInt(100) * 0.01f;
        animate()
                .scaleX(value)
                .scaleY(value)
                .setDuration(10000)
                .start();
    }
}
