package com.daytoplay.views;

import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;

import com.daytoplay.R;
import com.daytoplay.activities.FeedActivity;
import com.google.android.gms.ads.NativeExpressAdView;

public class AdItemHolder extends RecyclerView.ViewHolder {
    public final View view;
    private static Handler handler;
    private static final int REFRESH_AD = 1;
    private final NativeAd ad;

    public AdItemHolder(View view) {
        super(view);
        this.view = view;
        RelativeLayout adContainer = (RelativeLayout) view.findViewById(R.id.ad_holder);
        handler = new MessageHandler();
        FeedActivity activity = getFeedActivity(view);
        ad = activity.getAd();
        NativeExpressAdView adView = ad.getView();
        activity.removeInitialAd();
        adContainer.addView(adView);
    }

    private static FeedActivity getFeedActivity(View view) {
        return (FeedActivity) view.getContext();
    }

    public void refreshAd(int i) {
        ad.hide();
        handler.removeMessages(REFRESH_AD);
        handler.sendMessageDelayed(Message.obtain(handler, REFRESH_AD, ad), 500);
    }

    private static class MessageHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == REFRESH_AD) {
                NativeAd adView = (NativeAd) msg.obj;
                if (adView != null) {
                    adView.refresh();
                }
            }
        }
    }


}
