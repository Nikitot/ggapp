package com.daytoplay.views;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.daytoplay.R;
import com.daytoplay.utils.Utils;

public class ChannelItemHolder extends RecyclerView.ViewHolder {
    public final View view;
    public final TextView title;
    public final TranslateDraweeView9x16 logo;

    public ChannelItemHolder(View view) {
        super(view);
        this.view = view;
        title = (TextView) view.findViewById(R.id.title);
        logo = (TranslateDraweeView9x16) view.findViewById(R.id.channel_image);
        Utils.roundCrop(logo);
    }

    @Override
    public String toString() {
        return super.toString() + " '" + title.getText() + "'";
    }


}
