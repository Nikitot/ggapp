package com.daytoplay.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;

import com.daytoplay.R;
import com.daytoplay.activities.SplashActivity;
import com.daytoplay.activities.FeedActivity;
import com.daytoplay.utils.Logger;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

public class GGFirebaseMessagingService extends FirebaseMessagingService {

    public static final String TITLE = "title";
    public static final String MESSAGE = "message";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Logger.i(this, "From: " + remoteMessage.getFrom());
        Map<String, String> data = remoteMessage.getData();
        if (data != null) {
            Logger.i(this, "Notification message received");
            sendNotification(data);
        } else {
            Logger.i(this, "Notification is null");
        }
    }

    private void sendNotification(Map<String, String> data) {
        String title = data.get(TITLE);
        String text = data.get(MESSAGE);
        NotificationCompat.Builder builder = new NotificationCompat
                .Builder(this)
                .setSmallIcon(R.drawable.ic_stat)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher));
        builder.setContentTitle(title != null ? title : getString(R.string.app_name));
        if (text != null) {
            builder.setContentText(text);
        }

        Intent resultIntent = new Intent(this, SplashActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(FeedActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(resultPendingIntent);
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, builder.build());
    }
}

