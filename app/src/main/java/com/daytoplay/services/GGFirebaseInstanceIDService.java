package com.daytoplay.services;

import android.content.Context;
import android.support.annotation.NonNull;

import com.daytoplay.Config;
import com.daytoplay.Connector;
import com.daytoplay.ErrorType;
import com.daytoplay.utils.Logger;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.gson.JsonObject;

import okhttp3.ResponseBody;
import retrofit2.Response;

public class GGFirebaseInstanceIDService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Logger.i(this, "Refreshed token: " + refreshedToken);
        if (refreshedToken != null) {
            registerToken(this, refreshedToken);
        }
    }

    public static void registerToken(@NonNull final Context context, @NonNull final String token) {
        Connector.createService()
                .registerToken(createTokenJson(token).toString()).enqueue(new Connector.Callback<ResponseBody>(context) {
            @Override
            protected void onSuccess(Response<ResponseBody> response, String url) {
                if (response.isSuccessful()) {
                    Config.token = token;
                    Logger.i(context, "Firebase token registration success: " + token);
                } else {
                    Config.token = null;
                    Logger.i(context, "Firebase token registration failed: " + token);
                }
            }

            @Override
            protected void onError(ErrorType errorType, String msg, String url) {
                super.onError(errorType, msg, url);
                Config.token = null;
            }
        });
    }

    @NonNull
    private static JsonObject createTokenJson(String token) {
        JsonObject jsonToken = new JsonObject();
        JsonObject jsonTokenResult = new JsonObject();
        jsonToken.addProperty("token", token);
        jsonTokenResult.add("client_registration_token", jsonToken);
        return jsonTokenResult;
    }

}
