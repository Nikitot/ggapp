package com.daytoplay.items;

import java.sql.Timestamp;

public class LikeItem {

    public final long id;
    public final long user_id;
    public final long news_item_id;
    public final Timestamp created_at;
    public final Timestamp updated_at;

    public LikeItem(long id, long user_id, long news_item_id, Timestamp created_at, Timestamp updated_at) {
        this.id = id;
        this.user_id = user_id;
        this.news_item_id = news_item_id;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }
}
