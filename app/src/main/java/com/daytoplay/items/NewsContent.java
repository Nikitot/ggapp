package com.daytoplay.items;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;

import com.gg.api.Channel;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class NewsContent {
    public static final String ROOT_LINK = "root_link";
    public final List<NewsItem> items = Collections.synchronizedList(new ArrayList<NewsItem>());

    public void addItem(NewsItem item) {
        items.add(item);
    }

    public List<NewsItem> getItems() {
        return items;
    }

    public int getSize() {
        return items.size();
    }

    public NewsItem getItem(int position) {
        return items.get(position);
    }

    public void addEnd(List<NewsItem> items) {
        this.items.addAll(items);
    }

    public void addTop(List<NewsItem> updatedItems) {
        items.addAll(0, updatedItems);
    }

    public void updateItem(NewsItem item, int position) {
        items.set(position, item);
    }

    public static class NewsItem implements Parcelable {
        public final long id;
        public final String link;
        public final String title;
        public final String thumbnail_url;
        public final String details;
        public final Timestamp created_at;
        public final int news_item_type;
        public int views_count;
        public int likes_count;

        @Nullable
        public Channel channel;

        public NewsItem(int id, String link, String title,
                        String thumbnail_url, String details, int news_item_type, Timestamp created_at, int views_count, int likes_count) {
            this.id = id;
            this.link = link;
            this.title = title;
            this.thumbnail_url = thumbnail_url;
            this.details = details;
            this.news_item_type = news_item_type;
            this.created_at = created_at;
            this.views_count = views_count;
            this.likes_count = likes_count;
        }

        protected NewsItem(Parcel in) {
            id = in.readLong();
            link = in.readString();
            title = in.readString();
            thumbnail_url = in.readString();
            details = in.readString();
            news_item_type = in.readInt();
            created_at = (Timestamp) in.readSerializable();
            views_count = in.readInt();
            likes_count = in.readInt();
            channel = (Channel) in.readSerializable();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(id);
            dest.writeString(link);
            dest.writeString(title);
            dest.writeString(thumbnail_url);
            dest.writeString(details);
            dest.writeInt(news_item_type);
            dest.writeSerializable(created_at);
            dest.writeInt(views_count);
            dest.writeInt(likes_count);
            dest.writeSerializable(channel);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<NewsItem> CREATOR = new Creator<NewsItem>() {
            @Override
            public NewsItem createFromParcel(Parcel in) {
                return new NewsItem(in);
            }

            @Override
            public NewsItem[] newArray(int size) {
                return new NewsItem[size];
            }
        };

        public String getCreatedDate() {
            return SimpleDateFormat.getDateInstance().format(created_at);
        }
    }
}
