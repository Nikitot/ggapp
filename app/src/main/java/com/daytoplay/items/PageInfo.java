package com.daytoplay.items;

import android.content.Context;

import com.daytoplay.R;
import com.daytoplay.activities.FeedActivity;

public class PageInfo {
    public final int icon;
    public final String text;
    public final FeedActivity.SectionType type;

    public PageInfo(Context context, FeedActivity.SectionType type) {
        this.type = type;
        switch (type) {
            case NEW:
                icon = R.drawable.ic_newspaper_white_24dp;
                text = context.getResources().getString(R.string.headlines);
                break;
//            case TOP:
//                icon = R.drawable.ic_whatshot_white_24dp;
//                text = context.getResources().getString(R.string.home);
//                break;
            case LIKED:
                icon = R.drawable.ic_favorite_white_24dp;
                text = context.getResources().getString(R.string.favorites);
                break;
            default:
                throw new RuntimeException("Unknown type" + type);

        }
    }
}