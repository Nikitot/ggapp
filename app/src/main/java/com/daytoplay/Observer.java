package com.daytoplay;

public interface Observer {
    void objectModified(Object obj);
}
