package com.daytoplay;

import android.support.multidex.MultiDexApplication;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.daytoplay.utils.Logger;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.crash.FirebaseCrash;

public class App extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        Fresco.initialize(this);
        Logger.createFirebaseAnalytics(this);
        MobileAds.initialize(this, getString(R.string.ad_app_id));
        Config.initialize(this);

        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable e) {
                if (BuildConfig.REPORT_CRASH) {
                    FirebaseCrash.report(e);
                }
            }
        });
    }
}
