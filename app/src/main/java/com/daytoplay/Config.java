package com.daytoplay;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import com.gg.api.Channel;
import com.daytoplay.utils.Logger;
import com.daytoplay.utils.Utils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import java.util.ArrayList;
import java.util.List;

public class Config {

    private static final Observers<Observer> observers = new Observers<>();
    public static final String YOUTUBE_API_KEY = "AIzaSyAFeZxZaLSpDS-ns-PGgnGMo0Fx7pVBjyY";
    public static final int VK_API_KEY = 5507780;
    public static final String PORT_COUNT = "g257222J5sdoif1c43bgoibdhs376636fpigsd";
    private static final String APP_PREFERENCES = "APP_PREFERENCES";
    private static final int[] ports = new int[]{4000, 2001, 2002};
    public static String token;
    public static String deviceId;
    public static int adPeriod;
    public static Point defaultSize;
    private int cacheExpiration;
    public final List<Channel> channels = new ArrayList<>();
    public final FirebaseRemoteConfig remote;
    public static final FirebaseAuth auth = FirebaseAuth.getInstance();
    private static final Config config = new Config();
    private Context context;

    @SuppressLint("HardwareIds")
    public static Config initialize(@NonNull Context context) {
        config.setContext(context);
        deviceId = Utils.md5(Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID)).toUpperCase();
        DisplayMetrics metrics = new DisplayMetrics();
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        wm.getDefaultDisplay().getMetrics(metrics);
        defaultSize = metrics.heightPixels > metrics.widthPixels ?
                new Point(metrics.widthPixels, metrics.heightPixels) :
                new Point(metrics.heightPixels, metrics.widthPixels);
        return config;
    }

    public Config() {
        remote = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build();
        remote.setConfigSettings(configSettings);
        remote.setDefaults(R.xml.remote_config_defaults);
        cacheExpiration = 3600;

        if (remote.getInfo().getConfigSettings().isDeveloperModeEnabled()) {
            cacheExpiration = 0;
        }
    }

    public static Config get() {
        return config;
    }

    public static boolean hasUser() {
        return Config.auth.getCurrentUser() != null;
    }

    public void fetchRemote(final AppCompatActivity activity) {
        remote.fetch(cacheExpiration)
                .addOnCompleteListener(activity, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            remote.activateFetched();
                            initRemoteValues(activity);
                        }
                    }
                });
    }

    private void initRemoteValues(Context context) {
        String adPeriodStr = "ad_native_feed";
        try {
            adPeriod = Integer.parseInt(remote.getString(adPeriodStr));
        } catch (NumberFormatException ex) {
            Logger.e(context, "parameter" + adPeriodStr + "must be int!");
        }
    }

    public static boolean isShowAd() {
        return adPeriod > 0;
    }

    @Nullable
    public static SharedPreferences getSharedPreferences() {
        Context applicationContext = config.getContext();
        if (applicationContext != null) {
            return applicationContext.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        }
        return null;
    }

    public static int getPortPosition() {
        final SharedPreferences sharedPreferences;
        if (config.getContext() != null) {
            sharedPreferences = getSharedPreferences();
            if (sharedPreferences != null) {
                return sharedPreferences.getInt(PORT_COUNT, 0);
            }
        }
        return 0;
    }

    public static void setPortPosition(int position) {
        SharedPreferences sharedPreferences = getSharedPreferences();
        if (sharedPreferences != null) {
            sharedPreferences
                    .edit()
                    .putInt(PORT_COUNT, position)
                    .apply();
        }
        Connector.resetPort(ports[position]);
    }

    public static int getPort() {
        return ports[getPortPosition()];
    }

    public static void registerObserver(Observer observer) {
        observers.add(observer);
    }

    public static void unregisterObserver(Observer observer) {
        if (observers.contains(observer)) {
            observers.remove(observer);
        }
    }

    public static void notifyObservers(Object obj) {
        observers.notifyObjectModified(obj);
    }

    public static Channel getChannel(int id) {
        for (Channel channel : config.channels) {
            if (channel.getId() == id) {
                return channel;
            }
        }
        return null;
    }

    public static List<Channel> getChannels() {
        return config.channels;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public Context getContext() {
        return context;
    }
}
