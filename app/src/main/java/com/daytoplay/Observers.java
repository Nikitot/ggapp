package com.daytoplay;

import java.util.ArrayList;

public class Observers<T extends Observer> extends ArrayList<T> {
    public void notifyObjectModified(Object obj) {
        for (T o : this) o.objectModified(obj);
    }
}
